# Introducción
En este repositorio guardo material y scripts misceláneos que me sirven en la universidad, tales como tareas de programación y utilidades varias.

Todo este software está licenciado bajo la [GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0.en.html) (vea [COPYING](COPYING)), por lo tanto, es [software libre](https://www.gnu.org/philosophy/free-sw.es.html). Esto significa que los usuarios gozan de las cuatro libertades esenciales:

* La libertad de ejecutar el programa como se desee, con cualquier propósito (libertad 0).
* La libertad de estudiar cómo funciona el programa, y cambiarlo para que haga lo que se desee (libertad 1). El acceso al código fuente es una condición necesaria para ello.
* La libertad de redistribuir copias para ayudar a otros (libertad 2).
* La libertad de distribuir copias de sus versiones modificadas a terceros (libertad 3). Esto le permite ofrecer a toda la comunidad la oportunidad de beneficiarse de las modificaciones. El acceso al código fuente es una condición necesaria para ello.

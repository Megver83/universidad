#!/usr/bin/python
# Autor: David Pizarro <megver83@hyperbola.info>
# Escrito y probado desde GNU/Linux, desconozco su comportamiento en Windows.
#
# Características adicionales a las exigidas de este script:
# * Exige que el input tenga cinco palabras.
# * Las entradas que deben ser números fallarán si no están dentro del rango
#   correspondiente, dependiendo si son mes/día/año (ej. el mes 13 fallará).
# * Los porcentajes totales mostrados son números enteros (usé integrers en vez de floats por estética).
# * Absolutamente todo se guarda en los distintos tipos de variables de python.
# * Este programa fallará si no hay frutas (total 0)
# * Utilicé algunos caracteres fancys para el output :D

# Variables
lista=''
year=int()
years=[]
years_list=[]
month=int()
months={}              # months[año] = meses separados por espacios
day=int()
fruit=''
fruits=["naranja", "manzana", "pera", "kiwi"]
fruits_years_list={}   # fruits_years_list[año] = cantidad de frutas anual
fruits_months_list={}  # fruits_months_list[año_mes] = cantidad de frutas mensual
fruits_total_list={"naranja" : 0, "manzana" : 0, "pera" : 0, "kiwi" : 0}
lista_completa=''
end=int()
total=int()
valid_dates=[]

# Aquí empieza la diversión
print("Ingrese una(s) lista(s) separada(s) por espacios (dejar en blanco para terminar).")
print("Frutas válidas: naranja, manzana, pera, kiwi.")
print("Los porcentajes totales mostrados al final son números enteros aproximados.")

while True:
  # Crea la lista
  lista = input("Ingresar lista: ")
  if lista == '':
      break

  # Transforma en minúsculas el nombre de la fruta
  lista = lista.lower()

  # Variables de _lista[n], siendo n:
  # 0 = año
  # 1 = mes
  # 2 = día
  # 3 = fruta
  # 4 = cantidad de frutas
  _lista = lista.split()

  # Begin sanity checks
  if len(_lista) != 5:             # Check list length
      print("Error: Debes introducir solo cinco elementos en la lista."); continue

  if not _lista[0].isdigit(): print("Error: El año debe ser un número natural."); continue
  if int(_lista[0]) < 0: print("Error: El año debe ser un número natural."); continue
  if not _lista[1].isdigit(): print("Error: El mes debe ser un número natural."); continue
  if int(_lista[1]) > 12 or int(_lista[0]) < 1: print("Error: El mes debe ser un número natural entre 1 y 12."); continue
  if not _lista[2].isdigit(): print("Error: El día debe ser un número natural."); continue
  if int(_lista[2]) > 31 or int(_lista[0]) < 1: print("Error: El día debe ser un número natural entre 1 y 31."); continue
  if not _lista[4].isdigit(): print("Error: El número de frutas debe ser un número natural."); continue
  if int(_lista[4]) < 0: print("Error: El número de frutas debe ser un número natural."); continue

  if year == int(_lista[0]):       # Check year
      if month == int(_lista[1]):  # Check month
        if day > int(_lista[2]):   # Check day
          print("Error: El día de esta lista no puede ser menor que el anterior"); continue
      elif month > int(_lista[1]):
        print("Error: El mes de esta lista no puede ser menor que el anterior"); continue
  elif year > int(_lista[0]):
      print("Error: El año de esta lista no puede ser menor que el anterior"); continue

  if not _lista[3] in fruits:      # Check fruit
      print("Error: fruta inválida"); continue
  # End sanity checks

  # Actualizar variables
  year,month,day = int(_lista[0]),int(_lista[1]),int(_lista[2])
  lista_completa += ' ' + lista

# Algo de aire entre tantas letras...
print()

# Transforma el string a list
lista_completa = lista_completa.split()

# Cantidad de listas introducidas
list_cant = int(len(lista_completa)/5)

# Reinicia variables a usar
year=int()
month=int()

# Obtener lista de años con meses
for n in range(0,int(len(lista_completa)),5):
    years.append(lista_completa[n])
    years.append(lista_completa[n + 1])
    years_list.append(lista_completa[n])

years_list = list(set(years_list))
years_list.sort()

# Crea lista de meses por año
for y in years_list:
    months[y] = '0'

# Cantidad de años
list_years = int(len(years)/2)

# Crea las listas de cantidades de frutas por años y meses, usando diccionarios de python
for y in range(list_years):
    begin = end
    end = begin + 2
    tmp_years = years[begin:end]

    for fruit in fruits:
        fruits_months_list[str(fruit) + '_' + str(tmp_years[0]) + '_' + str(tmp_years[1])] = 0
        fruits_years_list[str(fruit) + '_' + str(tmp_years[0])] = 0
        valid_dates.append(str(fruit) + '_' + str(tmp_years[0]) + '_' + str(tmp_years[1]))

# Ahora que las listas existen, completarlas
end=int()
for list_num in range(list_cant):
    begin = end
    end = begin + 5
    tmp_list = lista_completa[begin:end]

    # Suma las frutas y guardalas en sus respectivas listas
    fruits_months_list[tmp_list[3] + '_' + tmp_list[0] + '_' + tmp_list[1]] = \
        int(fruits_months_list[tmp_list[3] + '_' + tmp_list[0] + '_' + tmp_list[1]]) + int(tmp_list[4])
    fruits_years_list[tmp_list[3] + '_' + tmp_list[0]] = \
        int(fruits_years_list[tmp_list[3] + '_' + tmp_list[0]]) + int(tmp_list[4])
    fruits_total_list[tmp_list[3]] = int(fruits_total_list[tmp_list[3]]) + int(tmp_list[4])

    # Crea una lista de meses por año
    if months[tmp_list[0]] == 0:
        months[tmp_list[0]] = str(tmp_list[1])
    else:
        months[tmp_list[0]] = str(months[tmp_list[0]]) + ' ' + str(tmp_list[1])

# Elimina meses que se repiten por año
# m = año
# n = mes
for m in months:
    tmp_months = list(set(months[m]))
    tmp_months.sort()
    months[m] = 0
    for n in tmp_months:
        if months[m] == 0:
            months[m] = n
        else:
            months[m] = months[m] + ' ' + n

# Obtener el total de frutas
for i in fruits_total_list:
    total = int(fruits_total_list[i]) + total

# Falla si no hay frutas
if total == 0:
    print("Error: no hay frutas (total 0)"); exit(1)

# Mostrar resultados, separado por años y meses
if fruits_years_list != {}:
    for y in years_list:
        print("Frutas del año", y + ':')
        for f in fruits:
            if f == fruits[-1]:
                line = '└──'
                line1 = ' '
            else:
                line = '├──'
                line1 = '│'

            print(line, str(f).capitalize() + 's:', fruits_years_list[str(f) + '_' + str(y)])

            if fruits_years_list[str(f) + '_' + str(y)] != 0:
                months_list=[]

                # Crea una lista de meses con una cantidad de frutas distinta de cero
                for n in months[y].split():
                    if str(f) + '_' + str(y) + '_' + str(n) in valid_dates and fruits_months_list[str(f) + '_' + str(y) + '_' + str(n)] != 0:
                        months_list.append(n)

                # Usa dicha lista para declarar correctamente line2
                for n in months_list:
                    if n == months_list[-1]:
                        line2 = '  └──'
                    else:
                        line2 = '  ├──'

                    print(line1, line2, fruits_months_list[str(f) + '_' + str(y) + '_' + str(n)], "en el mes", n)
        print()

# Mostrar totalidad de resultados
print("Total:", total,
      "\nNaranjas:", fruits_total_list['naranja'], "(" + str(int(int(fruits_total_list['naranja'])/total * 100)) + "%)",
      "\nManzanas:", fruits_total_list['manzana'], "(" + str(int(int(fruits_total_list['manzana'])/total * 100)) + "%)",
      "\nPeras:", fruits_total_list['pera'], "(" + str(int(int(fruits_total_list['pera'])/total * 100)) + "%)",
      "\nKiwis:", fruits_total_list['kiwi'], "(" + str(int(int(fruits_total_list['kiwi'])/total * 100)) + "%)")

#!/usr/bin/python
# Autor: David Pizarro <david.pizarro@alumnos.ucn.cl>

# Variables
total_j1 = 0
total_j2 = 0
file = open("dados.txt", "r")
lineas = file.read().splitlines()

# Cierra el archivo
file.close()

# Funciones para la obtención y cálculo de puntos
def calcularPuntaje(puntaje):
    puntos=0
    if puntaje == 7 or puntaje == 11:
        puntos = 3
    elif puntaje == 2 or puntaje == 3 or puntaje == 12:
        puntos = -2
    elif puntaje == 4 or puntaje == 5 or puntaje == 6 or puntaje == 8 or puntaje == 9 or puntaje == 10:
        puntos = 1
    return puntos

def obtenerPuntos(linea,jugador):
    numeros = linea[1].split(',')
    if int(numeros[0]) == jugador:
        return int(numeros[1]) + int(numeros[2])
    else: return 0

def ganador():
    if total_j1 == total_j2:
        print("Es un empate")
    elif total_j1 > total_j2:
        print("El ganador es el jugador 1")
    elif total_j1 < total_j2:
        print("El ganador es el jugador 2")

# Funciones misc
def advertencia(msg,line):
    print("ADVERTENCIA: %s\nLínea inválida ignorada: %s" % (msg,line))

def revisar_dados(n,line):
    if int(n) < 1 or int(n) > 6:
        advertencia("El dado debe ser un número natural entre 1 y 6",line)
        return True
    else: return False

# Aquí empieza la magia
for x in lineas:
    x_str = x
    x = x.split()
    if x[0].lower() != 'jugador' or len(x) != 2:
        advertencia("La línea debe empezar con la palabra 'jugador' y tener tres números separados por comas", x_str)
        continue

    # numeros[0] es el número del jugador
    # numeros[1] es el puntaje del primer dado
    # numeros[2] es el puntaje del segundo dado
    numeros = x[1].split(',')
    if len(numeros) == 3:
        for n in numeros:
            if not n.isdigit():
                advertencia("Los caracteres separados por comas deben ser números naturales", x_str)
                Continue = True
                break
            else: Continue = False
        if Continue: continue
    else:
        advertencia("Los números separados por comas deben ser solo tres números naturales", x_str)
        continue

    if revisar_dados(numeros[1],x_str) or revisar_dados(numeros[2],x_str): continue

    if int(numeros[0]) == 1 or int(numeros[0]) == 2:
        pass
    else:
        advertencia("El jugador número "'numeros[0]'" no existe! Solo se consideran dos", x_str)
        continue

    # Obtener puntos de los dados, toma la línea como único argumento
    PuntosJ1 = obtenerPuntos(x,1)
    PuntosJ2 = obtenerPuntos(x,2)

    # Calcular total de puntos según las reglas del juego
    total_j1 = total_j1 + calcularPuntaje(obtenerPuntos(x,1))
    total_j2 = total_j2 + calcularPuntaje(obtenerPuntos(x,2))

# Imprime resultados
print("Puntaje final del jugador 1:", total_j1)
print("Puntaje final del jugador 2:", total_j2)
ganador()

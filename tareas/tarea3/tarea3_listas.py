#!/usr/bin/python
'Calculate XP and level of New World players (lists-only version)'

# Materials and products recipes
recipes=(# [0] Resources, [1] Products
(# [0]=resource, [1]=new material, [2]=resources quantity
    ('Jabalí', 'Piel', 20),
    ('Lobo', 'Piel', 10),
    ('Pavo', 'Piel', 5),
    ('Madera dura', 'Madera', 30),
    ('Madera de sino', 'Madera', 60),
    ('Pimpollo', 'Madera', 45),
    ('Cáñamo', 'Fibra', 20),
    ('Hierbasedosa', 'Fibra', 15),
    ('Especias', 'Fibra', 10)
),
(# [0]=product, [1]=used material, [2]=materials quantity
    ('Cuero', 'Piel', 4),
    ('Tablones', 'Madera', 5),
    ('Tela', 'Fibra', 10)
))
# Usernames list and data (same indexes)
db, userdb = [], []

# Generate materials lists from recipes to share the same indexes
matter = [()] * 3 # [0] Resources, [1] Products, [2] Materials
for i, recipes_tuple in enumerate(recipes):
    matter[i] = tuple(item[0] for item in recipes_tuple)
matter[2] = 'Piel', 'Madera', 'Fibra'

def getlevel(level, exp):
    'Check if there is enough xp for level'
    required_xp=50

    if level > 0:
        next_level=1
        while level >= next_level:
            required_xp += round(required_xp*1.05)
            next_level  += 1

    if required_xp > exp:
        return False
    return True

def bestwood():
    'Return who did a better use to the wood and wood average'
    wood, total_planks = [], 0
    planks_i = matter[1].index('Tablones')
    wood_i   = matter[2].index('Madera')

    for player_name in db:
        player_db_i   = db.index(player_name)
        total_planks += userdb[player_db_i][1][planks_i]
        wood.append(userdb[player_db_i][2][wood_i])

    # WARNING: if more than one player has min(wood), consider only the first one in the list
    for player_name in db:
        if userdb[db.index(player_name)][2][wood_i] == min(wood):
            break

    return [player_name, total_planks/len(db)]

def table(table_title, player_i, titles_row):
    'Prints component stats as a table'
    row = f"{'JUGADOR':<20}"

    print(table_title)
    for title in titles_row:
        row += f"{title.upper():<15}"
    print(row)

    for player_name in db:
        player_row = f"{player_name:<20}"
        for number in userdb[db.index(player_name)][player_i]:
            player_row += f"{number:<15}"
        print(player_row)
    print('---')

with open('historial.txt', 'r', encoding='utf-8') as file:
    for line in file:
        line = line.strip().split(',')
        player, element = line[0], line[1]

        if not player in db:
            db.append(player)
            userdb.append([[0, 0],        # [0] [XP, Level]
                [0 for i in matter[1]],   # [1] Products (same matter[1] indexes)
                [0 for i in matter[2]]])  # [2] Materials (same matter[2] indexes)

        user_index = db.index(player)

        if element in matter[0]:
            element_index  = matter[0].index(element)

            # New material
            new_material   = recipes[0][element_index][1]
            quantity       = recipes[0][element_index][2]
            material_index = matter[2].index(new_material)

            userdb[user_index][2][material_index] += quantity
            userdb[user_index][0][0] += 25 + (userdb[user_index][0][1] * 10)
        elif element in matter[1]:
            element_index = matter[1].index(element)

            # Used material
            used_material  = recipes[1][element_index][1]
            quantity       = recipes[1][element_index][2]
            material_index = matter[2].index(used_material)

            # New product
            new_product   = recipes[1][element_index][1]
            product_index = matter[2].index(new_product)

            while userdb[user_index][2][material_index] >= quantity:
                userdb[user_index][2][material_index] -= quantity
                userdb[user_index][1][product_index]  += 1
                userdb[user_index][0][0] += 10

        while getlevel(userdb[user_index][0][1], userdb[user_index][0][0]):
            userdb[user_index][0][1] += 1

table('1. Fabricación de productos', 1, matter[1])
table('2. Experiencia final', 0, ('Experiencia', 'Nivel'))
table('3. Materiales sobrantes', 2, matter[2])
print('4. El jugador que mejor aprovechó la madera fue:', bestwood()[0], '\n---')
print('5. Promedio de los Tablones fabricados:', int(bestwood()[1])) # Approximate average

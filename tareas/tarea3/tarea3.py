#!/usr/bin/python
'Calculate experience and level of New World players'

# resources[name]=(generated material, new material quant)
resources={
    'Jabalí':('Piel', 20),
    'Lobo':('Piel', 10),
    'Pavo':('Piel', 5),
    'Madera dura':('Madera', 30),
    'Madera de sino':('Madera', 60),
    'Pimpollo':('Madera', 45),
    'Cáñamo':('Fibra', 20),
    'Hierbasedosa':('Fibra', 15),
    'Especias':('Fibra', 10)
}
# products[name]=(needed material, needed quant)
products={
    'Cuero':('Piel', 4),
    'Tablones':('Madera', 5),
    'Tela':('Fibra', 10)
}
# Users database
db={}

def getlevel(level, exp):
    'Check if there is enough xp for level'
    required_xp=50

    if level > 0:
        next_level=1
        while level >= next_level:
            required_xp += round(required_xp*1.05)
            next_level  += 1

    if required_xp > exp:
        return False
    return True

def bestwood():
    'Return who did a better use to the wood and wood average'
    wood, total_planks = [], 0

    for player_name in list(db):
        wood.append(db[player_name]['Madera'])
        total_planks += db[player_name]['Tablones']

    # list(wood) and list(db) indexes are the same
    # WARNING: if more than one player has min(wood), consider only the first one in the list
    player_index    = wood.index(min(wood))
    bestwood_player = list(db)[player_index]

    return [bestwood_player, total_planks/len(db)]

def table(table_title, *elements):
    'Prints component stats as a table'
    row = f"{'JUGADOR':<20}"

    print(table_title)
    for title in elements:
        row += f"{title.upper():<15}"
    print(row)

    for player_name in list(db):
        player_row = f"{player_name:<20}"
        for component in elements:
            player_row += f"{db[player_name][component]:<15}"
        print(player_row)
    print('---')

with open('historial.txt', 'r', encoding='utf-8') as file:
    for line in file:
        line = line.strip().split(',')
        player, element = line[0], line[1]

        if not player in db:
            db[player] = {}
            for i in list(products) + ['Experiencia', 'Nivel', 'Piel', 'Madera', 'Fibra']:
                db[player][i] = 0

        if element in list(resources):
            db[player][resources[element][0]] += resources[element][1]
            db[player]['Experiencia'] += 25 + (db[player]['Nivel'] * 10)
        elif element in list(products):
            while db[player][products[element][0]] >= products[element][1]:
                db[player][products[element][0]] -= products[element][1]
                db[player][element] += 1
                db[player]['Experiencia'] += 10

        while getlevel(db[player]['Nivel'], db[player]['Experiencia']):
            db[player]['Nivel'] += 1

table('1. Fabricación de productos', 'Cuero', 'Tablones', 'Tela')
table('2. Experiencia final', 'Experiencia', 'Nivel')
table('3. Materiales sobrantes', 'Piel', 'Madera', 'Fibra')
print('4. El jugador que mejor aprovechó la madera fue:', bestwood()[0], '\n---')
print('5. Promedio de los Tablones fabricados:', int(bestwood()[1])) # Approximate average

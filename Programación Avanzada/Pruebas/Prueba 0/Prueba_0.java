import java.util.Scanner;

public class Prueba_0 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] opciones = {
            "Crear matriz",
            "Poner valor",
            "Borrar columna",
            "Borrar fila",
            "Sumar",
            "Print",
            "Salir"
        };

        boolean noRomper = true;
        int[][] matriz = new int[0][0];
        int[][] nuevaMatriz;
        int cantFil = 0;
        int cantCol = 0;

        while(noRomper) {
            System.out.println("Menú de opciones\n----------------");
            for(int i = 0; i < opciones.length; i++) {
                System.out.printf("%d. %s%n", i + 1, opciones[i]);
            }
            System.out.print("Seleccione una opción: ");
            int seleccion = scan.nextInt();

            switch(seleccion) {
                case 1: // Crear matriz
                    System.out.print("Cantidad de filas: ");
                    cantFil = scan.nextInt();

                    System.out.print("Cantidad de columnas: ");
                    cantCol = scan.nextInt();

                    matriz = new int[cantFil][cantCol];
                    for(int i = 0; i < cantFil; i++) {
                        for(int j = 0; j < cantCol; j++) {
                            matriz[i][j] = 0;
                        }
                    }
                    break;
                case 2: // Poner un valor
                    System.out.print("Fila: ");
                    int fil = scan.nextInt();

                    System.out.print("Columna: ");
                    int col = scan.nextInt();

                    System.out.print("Valor: ");
                    int val = scan.nextInt();

                    matriz[fil][col] = val;
                    break;
                case 3: // Borrar columna
                    System.out.print("Columna: ");
                    int delCol = scan.nextInt();

                    nuevaMatriz = new int[cantFil][cantCol - 1];

                    for(int i = 0; i < cantFil - 2; i++) {
                        for(int j = 0; j < cantCol; j++) {
                            int colNew = j;

                            if(j == delCol) {
                                continue;
                            } else if(j > delCol) {
                                colNew = j - 1;
                            }

                            nuevaMatriz[i][colNew] = matriz[i][j];
                        }
                    }

                    cantCol -= 1;
                    matriz = new int[cantFil][cantCol];
                    matriz = nuevaMatriz;

                    break;
                case 4: // Borrar fila
                    System.out.print("Fila: ");
                    int delFil = scan.nextInt();

                    nuevaMatriz = new int[cantFil - 1][cantCol];

                    for(int i = 0; i < cantFil; i++) {
                        int filNew = i;

                        if(i == delFil) {
                            continue;
                        } else if(i > delFil) {
                            filNew = i - 1;
                        }

                        nuevaMatriz[filNew] = matriz[i];
                    }

                    cantFil -= 1;
                    matriz = new int[cantFil][cantCol];
                    matriz = nuevaMatriz;

                    break;
                case 5: // Sumar
                    int total = 0;
                    for(int[] fila: matriz) {
                        for(int i = 0; i < fila.length; i++) {
                            total += fila[i];
                        }
                    }
                    System.out.printf("Suma=%d", total);
                    break;
                case 6: // Imprime la matriz
                    for(int[] fila: matriz) {
                        for(int i = 0; i < fila.length; i++) {
                            System.out.printf("%d ", fila[i]);
                        }
                        System.out.println();
                    }
                    break;
                case 7: // Salir (rompe el bucle)
                    noRomper = false;
                    break;
                default:
                    System.out.println("Opción inválida");
            }
            System.out.println();
        }
    }
}

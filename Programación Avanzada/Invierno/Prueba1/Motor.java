/**
 * Almacena y maneja la información de motores de Avion.
 *
 * @since 2022-08-11
 * @author David Pizarro
 */
public class Motor {
	/**
	 * El nombre del motor
	 */
	private final String nombre;
	/**
	 * Primer día del avión donde se usará este motor
	 */
	private final int diaInicio;
	/**
	 * Último día del avión donde se usará este motor
	 */
	private final int diaFin;

	/**
	 * Inicializa los atributos del Motor.
	 *
	 * @param nombre El código del motor.
	 * @param inicio Día de inicio de funcionamiento del motor.
	 * @param fin Último día de funcionamiento del motor.
	 */
	public Motor(String nombre, int inicio, int fin) {
		this.nombre = nombre;
		this.diaInicio = inicio;
		this.diaFin = fin;
	}

	/**
	 * Retorna el nombre del motor.
	 *
	 * @return String nombre del motor
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Retorna el primer día de funcionamiento.
	 *
	 * @return int Inicio de la puesta en marcha del motor.
	 */
	public int getInicio() {
		return diaInicio;
	}

	/**
	 * Retorna el último día de funcionamiento.
	 *
	 * @return int Fin de la puesta en marcha del motor.
	 */
	public int getFin() {
		return diaFin;
	}
}

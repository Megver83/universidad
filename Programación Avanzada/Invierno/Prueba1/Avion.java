import java.util.*;

/**
 * La plantilla para crear aviones, gestionarlos y guardar su información.
 *
 * @since 2022-08-11
 * @author David Pizarro
 */
public class Avion {
	/**
	 * Una lista con todos los motores que han estado en el Avion.
	 */
	private ArrayList<Motor> historialMotores = new ArrayList<Motor>();
	/**
	 * La capacidad máxima de motores que soporta el avión.
	 * Su valor puede ser 2 o 3.
	 */
	private byte capacidadMotores;
	/**
	 * El código del avión
	 */
	private String nombre;

	/**
	 * Recibe un nombre y crea un avión.
	 *
	 * @param nombre El nombre del nuevo avión.
	 */
	public Avion(String nombre) {
		this.nombre = nombre;
		switch(nombre.charAt(1)) {
			case 'V':
				this.capacidadMotores = 2;
				break;
			case 'B':
				this.capacidadMotores = 3;
				break;
		}
	}

	/**
	 * Agrega un motor a la lista del historial.
	 *
	 * @param engine El objeto Motor a añadir.
	 */
	public void addEngine(Motor engine) {
		int inicio = engine.getInicio();
		int fin = engine.getFin();

		for(int dia = inicio; dia <= fin; dia++) {
			// Revisa día por día la cantidad de motores que están instalados
			int cantidadMotores = 0;
			for(Motor m : historialMotores) {
				// 'dia' está dentro del rango de este motor si se cumple:
				if(m.getInicio() <= dia && m.getFin() >= dia) cantidadMotores++;
			}
			// Configuracion invalida, se lo salta
			if(cantidadMotores > capacidadMotores) return;
		}

		historialMotores.add(engine);
	}

	/**
	 * Retorna el nombre del avión.
	 *
	 * @return String El nombre del avión.
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Retorna la lista de motores.
	 *
	 * @return ArrayList<Motor> La lista del historial de motores.
	 */
	public ArrayList<Motor> getMotores() {
		return historialMotores;
	}

	/**
	 * Imprime los motores que se utilizaban un determinado día.
	 *
	 * @param dia El día a estudiar.
	 */
	public void imprimirConfDia(int dia) {
		System.out.printf("*** Configuración de %s el día %d ***%n", nombre, dia);

		for(Motor m : historialMotores) {
			// 'dia' está dentro del rango de este motor si se cumple:
			if(m.getInicio() <= dia && m.getFin() >= dia) System.out.printf("Motor %s: le quedan %d días%n", m.getNombre(), m.getFin() - dia);
		}
	}

	/**
	 * Imprime y retorna la cantidad total de días que se usó cierto motor.
	 *
	 * @param nombreMotor El nombre del motor a estudiar.
	 * @return int Los días que se utilizó dicho motor.
	 */
	public int usoMotor(String nombreMotor) {
		int diasTotales = 0;

		for(Motor m : historialMotores) {
			if(m.getNombre().equals(nombreMotor)) {
				int inicio = m.getInicio();
				int fin = m.getFin();

				for(int dia = inicio; dia <= fin; dia++) {
					System.out.printf("Día %d instalado en %s%n", dia, nombre);
					diasTotales++;
				}
			}
		}
		return diasTotales;
	}

	/**
	 * Imprime una pseudo tabla con la lista de todos los motores que se usaron y sus días de uso.
	 */
	public void imprimirFullConf() {
		System.out.printf("*** Avión %s ***%n", nombre);
		
		// primer y ultimo día
		int primer = historialMotores.get(0).getInicio();
		int ultimo = historialMotores.get(0).getFin();
		for(Motor m : historialMotores) {
			if(m.getInicio() < primer) primer = m.getInicio();
			if(m.getFin() > ultimo) ultimo = m.getFin();
		}

		System.out.print("Motor  ");
		for(int i = primer; i <= ultimo; i++) {
			if(i < 10) {
				System.out.printf("0%d ", i);
			} else {
				System.out.printf("%d ", i);
			}
		}
		System.out.println();

		for(Motor m : historialMotores) {
			System.out.print(m.getNombre() + "     ");
			for(int dia = primer; dia <= ultimo; dia++) {
				// 'dia' está dentro del rango de este motor si se cumple:
				if(m.getInicio() <= dia && m.getFin() >= dia) {
					System.out.print("X  ");
					continue;
				}
				System.out.print("   ");
			}
			System.out.println();
		}
	}
}

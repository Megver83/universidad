import java.util.*;
import java.io.*;

/**
 * Sistema para manejar la mecánica de aviones de la señorita Marianela.
 *
 * @since 2022-08-11
 * @author David Pizarro
 */
public class Sistema {
	/**
	 * Lista donde se guardarán los aviones de la mecánica.
	 */
	private static ArrayList<Avion> aviones = new ArrayList<Avion>();

	/**
	 * Verifica que cierto avión existe en nuestros registros.
	 *
	 * @param nombre El nombre del avión.
	 * @return int El índice de dicho avión en la lista 'aviones', -1 si no existe.
	 */
	private static int avionPresente(String nombre) {
		for(int i = 0; i < aviones.size(); i++) {
			if(aviones.get(i).getNombre().equals(nombre)) return i;
		}
		return -1;
	}

	/**
	 * Verifica que un mismo motor no se instale simultáneamente en más de un avión.
	 *
	 * @param engine El objeto Motor a estudiar.
	 * @return boolean false si ya está instalado en otro avión en un rango de días igual o similar.
	 */
	private static boolean verificarMotor(Motor engine) {
		for(Avion a : aviones) {
			for(Motor m : a.getMotores()) {
				// Salta todos los motores que no sean 'engine'
				if(!engine.getNombre().equals(m.getNombre())) continue;

				for(int dia = engine.getInicio(); dia <= engine.getFin(); dia++) {
					// Revisa día por día si intersectan en alguno.
					// 'dia' está dentro del rango del motor 'm' si se cumple:
					if(m.getInicio() <= dia && m.getFin() >= dia) return false;
				}

			}
		}
		return true;
	}

	/**
	 * Carga el sistema desde un archivo de texto plano.
	 *
	 * @param nombreArchivo El nombre del archivo a leer.
	 */
	private static void cargarSistema(String nombreArchivo) throws IOException {
		Scanner scanObj = new Scanner(new File(nombreArchivo));
		while(scanObj.hasNextLine()) {
			String[] split = scanObj.nextLine().split(" ");

			// Agrega el avion
			Avion nuevoAvion;
			int avionIndex = avionPresente(split[1]);
			if(avionIndex < 0) {
				nuevoAvion = new Avion(split[1]);
				aviones.add(nuevoAvion);
			} else {
				nuevoAvion = aviones.get(avionIndex);
			}

			// Agrega el motor
			String[] days = split[2].split("-");
			int inicio = Integer.parseInt(days[0]);
			int fin = Integer.parseInt(days[1]);
			Motor nuevoMotor = new Motor(split[0], inicio, fin);
			if(verificarMotor(nuevoMotor)) nuevoAvion.addEngine(nuevoMotor);
		}
	}

	public static void main(String[] args) throws IOException {
		cargarSistema("aviones.txt");
		
		Scanner scan = new Scanner(System.in);
		String[] options = {
			"Ver configuracion de un avión en cierto día",
			"Ver el historial de un motor",
			"Ver la carta gantt de un avión",
			"Salir"
		};
		
		boolean repeat = true;
		while(repeat) {
			System.out.println("Bienvenido al mejor sistema de mecánica para aviones.");
			for(int i = 0; i < options.length; i++) {
				System.out.printf("%d. %s%n", i + 1, options[i]);
			}
			System.out.print("Escoja una opción: ");
			int opt = scan.nextInt();
			scan.nextLine();

			switch(opt) {
				case 1:
				case 3:
					System.out.print("Escriba el código del avión: ");
					String codAvion = scan.nextLine();
					
					int avionIndex = avionPresente(codAvion);
					if(avionIndex < 0) {
						System.out.println("Avión inválido.");
						break;
					}
					
					switch(opt) {
						case 1:
						System.out.print("Indique el número del día: ");
						int dia = scan.nextInt();
						aviones.get(avionIndex).imprimirConfDia(dia);
						break;
					case 3:
						aviones.get(avionIndex).imprimirFullConf();
						break;
					}

					break;
				case 2:
					System.out.print("Escriba el código del motor: ");
					String codMotor = scan.nextLine();
					
					int dias = 0;
					for(Avion a : aviones) {
						dias += a.usoMotor(codMotor);
					}

					if(dias == 0) {
						// Un motor que nunca se ha usado es porque no existe
						System.out.println("Motor inválido.");
						break;
					}

					double factorVidaUtil = 100;
					factorVidaUtil /= 365*5;

					System.out.printf("El motor %s ha sido usado por %d días%n", codMotor, dias);
					System.out.printf("Vida útil restante: %f%%%n", 365 * 5 * factorVidaUtil - dias * factorVidaUtil);
					break;
				case 4:
					repeat = false;
					break;
				default:
					System.out.println("Opción inválida.");
					break;
			}
			System.out.println();
		}
	}
}

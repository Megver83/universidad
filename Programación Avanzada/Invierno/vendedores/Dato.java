import java.util.*;
import java.io.*;

public class Dato {
    public final String nombre;
    public final int neto;
    public final int ganancias;
    public final ArrayList<String[]> viajes = new ArrayList<String[]>();

    public Dato(String[] data) {
        this.nombre = data[0];
        this.neto = Integer.parseInt(data[1]);

        // Inicializa con el último elemento, ya que el bucle no lo toma
        int ventas = Integer.parseInt(data[data.length - 1]);
        for(int i = 2; i <= data.length - 4; i += 2) {
            this.viajes.add(new String[]{data[i], data[i+2]});
            ventas += Integer.parseInt(data[i+1]);
        }
        this.ganancias = 12000 * ventas;
    }
}

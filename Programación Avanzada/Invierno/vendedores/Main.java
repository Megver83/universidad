import java.util.*;
import java.io.*;

public class Main {
    static ArrayList<Dato> salesData = new ArrayList<Dato>();
    static ArrayList<String> ciudadesValidas = new ArrayList<String>();

    // viajesValidos y distancias tendrán elementos correlacionados por el indice
    static ArrayList<String> viajesValidos = new ArrayList<String>();
    static ArrayList<Integer> distancias = new ArrayList<Integer>();

    public static void main(String[] args) throws IOException {
        File fileObj;
        Scanner scanObj;

        fileObj = new File("datos.txt");
        scanObj = new Scanner(fileObj);

        while (scanObj.hasNextLine()) {
            salesData.add(new Dato(scanObj.nextLine().split(",")));
        }
        scanObj.close();

        fileObj = new File("distancias.txt");
        scanObj = new Scanner(fileObj);

        while (scanObj.hasNextLine()) {
            String[] distData = scanObj.nextLine().split(",");
            viajesValidos.add(distData[0] + distData[1]);
            distancias.add(Integer.parseInt(distData[2]));
        }
        scanObj.close();

        fileObj = new File("ciudades.txt");
        scanObj = new Scanner(fileObj);

        while (scanObj.hasNextLine()) {
            ciudadesValidas.add(scanObj.nextLine());
        }
        scanObj.close();

        for(Dato dato : salesData) {
            boolean valid = true;
            System.out.print(dato.nombre + ": ");

            // Calcula el gasto en viajes
            int kilometros = 0;
            for(String[] parCiudad : dato.viajes) {
                if(!ciudadesValidas.contains(parCiudad[0])) {
                    System.out.println("la ciudad " + parCiudad[0] + " no existe");
                    valid = false;
                    break;
                }
                if(!ciudadesValidas.contains(parCiudad[1]))  {
                    System.out.println("la ciudad " + parCiudad[1] + " no existe");
                    valid = false;
                    break;
                }

                String concat1 = parCiudad[0] + parCiudad[1];
                String concat2 = parCiudad[1] + parCiudad[0];
                if(viajesValidos.contains(concat1)) {
                    kilometros += distancias.get(viajesValidos.indexOf(concat1));
                } else if(viajesValidos.contains(concat2)) {
                    kilometros += distancias.get(viajesValidos.indexOf(concat2));
                } else {
                    System.out.println("no hay camino entre " + parCiudad[0] + " y " + parCiudad[1]);
                    valid = false;
                    break;
                }
            }
            if(!valid) break;

            int gasto = 1000 * kilometros;
            int netoReal = dato.ganancias - gasto;

            if(netoReal == dato.neto) {
                System.out.println("ganancia correcta: " + netoReal);
            } else {
                System.out.println("ganancia incorrecta: dice " + dato.neto + " pero es " + netoReal);
            }
        }
    }
}

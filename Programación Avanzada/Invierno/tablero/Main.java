import java.util.*;
import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        if(args.length == 0) {
            System.out.println("Debe especificar un archivo");
            return;
        }
        Scanner scanObj = new Scanner(new File(args[0]));

        // Crea un tablero
        Tablero juego1 = new Tablero(scanObj, Integer.parseInt(scanObj.nextLine()));

        System.out.println("Tablero 1\n=======");
        System.out.println("Antes de la simulación:");
        juego1.mostrarTablero();

        System.out.println("\nDespués de la simulación:");
        juego1.simularAtaques();
        juego1.mostrarTablero();
    }
}

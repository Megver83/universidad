import java.util.*;

public class Tablero {
    private int size;
    private String[][] tablero;
    private ArrayList<Pieza> piezasList = new ArrayList<Pieza>();

    public Tablero(Scanner scanObj, int size) {
        this.tablero = new String[size][size];
        this.size = size;

        while(scanObj.hasNextLine()) {
            String[] data = scanObj.nextLine().split(",");

            String nombre = data[0].toUpperCase();
            int row = Integer.parseInt(data[1]);
            int col = Integer.parseInt(data[2]);

            if(row >= size || col >= size) {
                System.out.printf("La pieza %s en (%d, %d) se sale del tablero, saltando...%n", nombre, row, col);
                continue;
            }

            tablero[row][col] = nombre;
            piezasList.add(new Pieza(nombre, new int[]{row, col}));
        }
    }

    public void mostrarTablero() {
        for(int i = 0; i < tablero.length; i++) {
            for(int j = 0; j < tablero.length; j++) {
                String symbol = tablero[i][j];
                if(symbol == null) symbol = "O";
                System.out.print(symbol + " ");
            }
            System.out.println();
        }
    }

    public boolean casillaLibre(int row, int col) {
        if(tablero[row][col] == null || tablero[row][col] == "X") return true;
        return false;
    }

    // Ataques frontales
    private void atacarArriba(int[] pos) {
        int row = pos[0];
        int col = pos[1];

        for(--row; row >= 0; row--) {
            if(!casillaLibre(row, col)) break;
            tablero[row][col] = "X";
        }
    }

    private void atacarAbajo(int[] pos) {
        int row = pos[0];
        int col = pos[1];

        for(++row; row < size; row++) {
            if(!casillaLibre(row, col)) break;
            tablero[row][col] = "X";
        }
    }

    private void atacarDerecha(int[] pos) {
        int row = pos[0];
        int col = pos[1];

        for(++col; col < size; col++) {
            if(!casillaLibre(row, col)) break;
            tablero[row][col] = "X";
        }
    }

    private void atacarIzquierda(int[] pos) {
        int row = pos[0];
        int col = pos[1];

        for(--col; col >= 0; col--) {
            if(!casillaLibre(row, col)) break;
            tablero[row][col] = "X";
        }
    }

    // Ataques diagonales
    private void atacarArribaDerecha(int[] pos) {
        int row = pos[0] - 1;
        int col = pos[1] + 1;

        while(row >= 0 && col < size && casillaLibre(row, col)) {
            tablero[row][col] = "X";
            row--; col++;
        }
    }

    private void atacarArribaIzquierda(int[] pos) {
        int row = pos[0] - 1;
        int col = pos[1] - 1;

        while(row >= 0 && col >= 0 && casillaLibre(row, col)) {
            tablero[row][col] = "X";
            row--; col--;
        }
    }

    private void atacarAbajoDerecha(int[] pos) {
        int row = pos[0] + 1;
        int col = pos[1] + 1;

        while(row < size && col < size && casillaLibre(row, col)) {
            tablero[row][col] = "X";
            row++; col++;
        }
    }

    private void atacarAbajoIzquierda(int[] pos) {
        int row = pos[0] + 1;
        int col = pos[1] - 1;

        while(row < size && col >= 0 && casillaLibre(row, col)) {
            tablero[row][col] = "X";
            row++; col--;
        }
    }

    // Marcar todas las posibilidades
    public void simularAtaques() {
        for(Pieza piezaObj : piezasList) {
            switch(piezaObj.nombre) {
                case "T": // Torre
                    atacarArriba(piezaObj.posicion);
                    atacarAbajo(piezaObj.posicion);
                    atacarDerecha(piezaObj.posicion);
                    atacarIzquierda(piezaObj.posicion);
                    break;
                case "A": // Alfil
                    atacarArribaIzquierda(piezaObj.posicion);
                    atacarArribaDerecha(piezaObj.posicion);
                    atacarAbajoIzquierda(piezaObj.posicion);
                    atacarAbajoDerecha(piezaObj.posicion);
                    break;
                default:
                    System.out.println("ADVERTENCIA: Pieza desconocida: " + piezaObj.nombre);
                    break;
            }
        }
    }
}

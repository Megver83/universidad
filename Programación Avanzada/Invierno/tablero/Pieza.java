public class Pieza {
    public final String nombre;
    public final int[] posicion;

    public Pieza(String nombre, int[] posicion) {
        this.nombre = nombre;
        this.posicion = posicion;
    }
}

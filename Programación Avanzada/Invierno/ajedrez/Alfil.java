class Alfil extends Pieza {
    public Alfil(int fila, int col) {
        super(fila, col);
    }

    public String getTipo() {
        return "A";
    }

    public void realizarAtaque(Tablero t) {
        int f, c;

        // ↓→
        f = fila + 1;
        c = columna + 1;
        while(t.esValida(f, c) && t.estaDesocupada(f, c)) {
            t.marcarAtacada(f, c);
            f++; c++;
        }

        // ↑→
        f = fila - 1;
        c = columna + 1;
        while(t.esValida(f, c) && t.estaDesocupada(f, c)) {
            t.marcarAtacada(f, c);
            f--; c++;
        }

        // ←↓
        f = fila + 1;
        c = columna - 1;
        while(t.esValida(f, c) && t.estaDesocupada(f, c)) {
            t.marcarAtacada(f, c);
            f++; c--;
        }

        // ←↑
        f = fila - 1;
        c = columna - 1;
        while(t.esValida(f, c) && t.estaDesocupada(f, c)) {
            t.marcarAtacada(f, c);
            f--; c--;
        }
    }
}

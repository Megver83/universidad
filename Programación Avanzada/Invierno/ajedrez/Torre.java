class Torre extends Pieza {
    public Torre(int fila, int col) {
        super(fila, col);
    }

    public String getTipo() {
        return "T";
    }

    public void realizarAtaque(Tablero t) {
        int f, c;

        // →
        c = columna + 1;
        while(t.esValida(fila, c) && t.estaDesocupada(fila, c)) {
            t.marcarAtacada(fila, c);
            c++;
        }

        // ←
        c = columna - 1;
        while(t.esValida(fila, c) && t.estaDesocupada(fila, c)) {
            t.marcarAtacada(fila, c);
            c--;
        }

        // ↑
        f = fila - 1;
        while(t.esValida(f, columna) && t.estaDesocupada(f, columna)) {
            t.marcarAtacada(f, columna);
            f--;
        }

        // ↓
        f = fila + 1;
        while(t.esValida(f, columna) && t.estaDesocupada(f, columna)) {
            t.marcarAtacada(f, columna);
            f++;
        }
    }
}

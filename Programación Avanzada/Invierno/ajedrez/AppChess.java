import java.util.*;
import java.io.*;

public class AppChess {
    public static void main(String[] args) throws IOException {
        AppChess app = new AppChess();
        app.ejecutar();
    }

    private void ejecutar() throws IOException {
        Jarvis m = new Jarvis();

        leerArchivo(m);

        int n = m.getCantidadCasillasSinAtaque();
        System.out.println("Casillas sin ataque: " + n);
    }

    private void leerArchivo(Jarvis m) throws IOException {
        Scanner s = new Scanner(new File("tablero.txt"));
        int fila = 0;

        while(s.hasNextLine()) {
            String linea = s.nextLine();
            String[] partes = linea.split(",");

            if(fila == 0) m.configurar(partes.length);
            for(int columna = 0; columna < partes.length; columna++) {
                String p = partes[columna];

                if("_".equals(p)) continue;

                m.ponerPieza(fila, columna, p);
            }
            fila++;
        }

        s.close();
    }
}

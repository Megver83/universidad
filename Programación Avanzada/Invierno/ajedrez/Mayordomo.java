interface Mayordomo {
    /**
     * Retorna la cantidad de casilas que están sin ataque en el tablero.
     *
     * @return Un entero indicando la cnatidad de casillas que están
     *         sin ataque en el tablero.
     */
    public int getCantidadCasillasSinAtaque();

    /**
     * Configura el mayordomo para que trabaje sobre un tablero del tamaño
     * indicado. El tablero será de tamaño x tamaño celdas.
     *
     * @param tamaño Un entero que indica la conatidad de filas y columnas
     *               del tablero que usará el mayordomo.
     */
    public void configurar(int size);

    /**
     * Hace que el mayordomo ponga una pieza en la posición indicada.
     *
     * @param fila La fila donde se pondrá la pieza
     * @param columna La columna donde se pondrá la pieza
     * @param tipoPieza Un String representando el tipo de pieza
     */
    public void ponerPieza(int fila, int columna, String tipoPieza);
}

import java.util.*;

class Jarvis implements Mayordomo {
    private List<Pieza> piezas = new ArrayList<Pieza>();
    private int size;

    public int getCantidadCasillasSinAtaque() {
        Tablero t = new Tablero(size);

        for(int i = 0; i < 2; i++) {
            for(Pieza p : piezas) {
                if(i == 0) p.situar(t);
                if(i == 1) p.realizarAtaque(t);
            }
        }

        return t.getCantidadCasillasSinAtaque();
    }

    public void configurar(int size) {
        if(size <= 0) {
            System.out.println("El tamaño de la matriz debe ser mayor que cero");
            return;
        }
        this.size = size;
    }

    public void ponerPieza(int fila, int columna, String tipoPieza) {
        switch(tipoPieza) {
            case "A":
                piezas.add(new Alfil(fila, columna));
                break;
            case "T":
                piezas.add(new Torre(fila, columna));
                break;
            default:
                System.out.println("Tipo de pieza inválida: " + tipoPieza);
                break;
        }
    }
}

class Tablero {
    private int size;
    private String[][] tablero;

    public Tablero(int size) {
        this.size = size;
        this.tablero = new String[size][size];
    }

    public int getCantidadCasillasSinAtaque() {
        int num = 0;
        for(int f = 0; f < size; f++) {
            for(int c = 0; c < size; c++) {
                String symbol = tablero[f][c];
                if(tablero[f][c] == null) {
                    num++;
                    symbol = "O";
                }
                System.out.print(symbol + " ");
            }
            System.out.println();
        }
        return num;
    }

    public void ocupar(int fila, int columna, String tipo) {
        tablero[fila][columna] = tipo;
    }

    public boolean esValida(int fila, int col) {
        if(fila < 0 || fila >= size) return false;
        if(col < 0 || col >= size) return false;
        return true;
    }

    public boolean estaDesocupada(int fila, int c) {
        String txt = tablero[fila][c];
        if(txt == null) return true;
        if("X".equals(txt)) return true;
        return false;
    }

    public void marcarAtacada(int fila, int c) {
        tablero[fila][c] = "X";
    }
}

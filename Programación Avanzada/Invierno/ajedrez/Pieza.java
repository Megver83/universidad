abstract class Pieza {
    protected final int fila;
    protected final int columna;

    public Pieza(int fila, int columna) {
        this.fila = fila;
        this.columna = columna;
    }

    public void situar(Tablero t) {
        t.ocupar(fila, columna, getTipo());
    }

    abstract protected String getTipo();

    abstract public void realizarAtaque(Tablero t);
}

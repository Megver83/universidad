abstract class NodoOp {
    protected int nodoNum;

    NodoOp(int nodoNum) {
        this.nodoNum = nodoNum;
    }

    public int getNodoNum() {
        return nodoNum;
    }

    public abstract int calcular();
}

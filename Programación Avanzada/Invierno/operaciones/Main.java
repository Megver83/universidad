import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        Sistema system = new Sistema("input.txt");
        System.out.println(system.getNodoByIndex(system.listSize() - 1).calcular());
    }
}

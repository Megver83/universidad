import java.util.*;
import java.io.*;

public class Sistema implements InterfazSistema {
    private static ArrayList<NodoOp> nodoOps = new ArrayList<NodoOp>();

    public Sistema(String txtFile) throws IOException {
        Scanner scanObj = new Scanner(new File(txtFile));
        while(scanObj.hasNextLine()) {
            String[] partes = scanObj.nextLine().split(" ");
            int nodoNum = Integer.parseInt(partes[0]);
            String tipo = partes[1];

            switch(tipo) {
                case "N":
                    nodoOps.add(new Nodo(nodoNum, Integer.parseInt(partes[2])));
                    break;
                case "SUMA":
                case "RESTA":
                case "DIV":
                case "MULT":
                    int[] parNodo = {
                        Integer.parseInt(partes[2]),
                        Integer.parseInt(partes[3]),
                    };
                    nodoOps.add(new Operacion(nodoNum, parNodo, tipo));
                    break;
            }
        }
    }

    public static NodoOp getNodo(int nodeNum) {
        for(NodoOp n : nodoOps) {
            if(n.getNodoNum() == nodeNum) {
                return n;
            }
        }
        return null;
    }

    public int listSize() {
        return nodoOps.size();
    }

    public NodoOp getNodoByIndex(int index) {
        return nodoOps.get(index);
    }
}

public class Operacion extends NodoOp {
    private String tipoOperacion;
    private int[] ref;

    public Operacion(int nodoNum, int[] ref, String tipoOperacion) {
        super(nodoNum);
        this.ref = ref;
        this.tipoOperacion = tipoOperacion;
    }

    public int calcular() {
        int nodo1 = Sistema.getNodo(ref[0]).calcular();
        int nodo2 = Sistema.getNodo(ref[1]).calcular();

        switch(tipoOperacion) {
            case "SUMA":
                return nodo1 + nodo2;
            case "RESTA":
                return nodo1 - nodo2;
            case "DIV":
                return nodo1 / nodo2;
            case "MULT":
                return nodo1 * nodo2;
        }
        return 0;
    }
}

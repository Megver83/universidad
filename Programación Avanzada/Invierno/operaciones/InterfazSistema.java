interface InterfazSistema {
    int listSize();
    NodoOp getNodoByIndex(int index);
}

public class Nodo extends NodoOp {
    private int valor;

    public Nodo(int nodoNum, int valor) {
        super(nodoNum);
        this.valor = valor;
    }

    public int calcular() {
        return valor;
    }
}

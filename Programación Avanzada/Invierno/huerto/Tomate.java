class Tomate extends Planta {
    private final double masaXlitro = 7.5; // factor de crecimiento de masa x litro
    private final double masaCosecha = 75;
    private double masa = 0;

    public void regar(double litros) {
        masa += masaXlitro * litros;
    }

    public boolean esCosechable() {
        return masa >= masaCosecha;
    }
}

class Apio extends Planta {
    private final double masaXlitro = 40; // factor de crecimiento de masa x litro
    private final double masaCosecha = 400;
    private final double alturaXlitro = 1; // factor de aumento de altura x litro
    private double masa = 0;
    private double altura = 0;

    public void regar(double litros) {
        masa += masaXlitro * litros;
        altura += alturaXlitro * litros;
    }

    public boolean esCosechable() {
        return masa >= masaCosecha;
    }
}

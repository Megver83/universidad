abstract class Planta {
    static int contadorCosechas;
    abstract void regar(double litros);
    abstract boolean esCosechable();
}

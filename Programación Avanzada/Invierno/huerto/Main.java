import java.util.*;
import java.io.*;

public class Main {
    public static void main(String[] args) {
        Huerto h = new Huerto();
        List<Planta> cosecha = new ArrayList<Planta>();
        Scanner scan = new Scanner(System.in);

        String[] options = {
            "Apio",
            "Lechuga",
            "Tomate",
            "REGAR",
            "Salir"
        };

        boolean repeat = true;
        while(repeat) {
            System.out.println("*** Bienvenido a tu huerta, escoge que plantar ***");
            for(int i = 0; i < options.length; i++) {
                System.out.printf("%d. %s%n", i + 1, options[i]);
            }

            System.out.print("Seleccione una opción: ");
            String opt = scan.nextLine();
            switch(opt) {
                case "1": // Apio
                    h.agregarPlanta(new Apio());
                    break;
                case "2": // Lechuga
                    h.agregarPlanta(new Lechuga());
                    break;
                case "3": // Tomate
                    h.agregarPlanta(new Tomate());
                    break;
                case "4": // Realizar riego de 10L
                    while(h.getNumPlantas() > 0) {
                        h.regar(10);
                        cosecha.addAll(h.cosechar());
                    }

                    System.out.printf("Total cosechado: %d%n", Planta.contadorCosechas);
                    Planta.contadorCosechas = 0;
                    break;
                case "5": // Salir
                    repeat = false;
                    break;
                default:
                    break;
            }
            System.out.println();
        }
    }
}

class Lechuga extends Planta {
    private final double masaXlitro = 12; // factor de crecimiento de masa x litro
    private final double masaCosecha = 120;
    private double masa = 0;

    public void regar(double litros) {
        masa += masaXlitro * litros;
    }

    public boolean esCosechable() {
        return masa >= masaCosecha;
    }
}

import java.util.*;

class Huerto {
    private List<Planta> plantasList = new ArrayList<Planta>();

    public void agregarPlanta(Planta p) {
        plantasList.add(p);
    }

    public void regar(double litros) {
        for(Planta p : plantasList) {
            p.regar(litros);
        }
    }

    public List<Planta> cosechar() {
        List<Planta> cosecha = new ArrayList<Planta>();
        Iterator<Planta> it = plantasList.iterator();
        while (it.hasNext()) {
            Planta p = (Planta) it.next();
            if(p.esCosechable()) {
                cosecha.add(p);
                it.remove();
                p.contadorCosechas++;
                break;
            }
        }

        return cosecha;
    }

    public int getNumPlantas() {
        return plantasList.size();
    }
}

public class Tableta extends Medicamento {
//     public final String nombre;
//     public final String codigo;
//     public final double precio;
    public final int cantidad;
    public final int mg;

    public Tableta(String n, String c, int p, int mg, int cantidad) {
        super(n, c, p);
        this.cantidad = cantidad;
        this.mg = mg;
    }

    public double precioReal(int p) {
        return p * 1.3;
    }

    public void imprimirDatos() {
        System.out.printf(
            "  Medicamento: %s%n" +
            "    Código: %s%n" +
            "    Precio: $%f%n" +
            "    Cantidad: %d%n" +
            "    Mg: %d%n",
            nombre, codigo, precio, cantidad, mg
        );
    }
}

public class Paciente {
    public final String nombre;
    public final String apellido;
    public final String rut;
    public final byte edad;

    public Paciente(String nombre, String apellido, String rut, byte edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.rut = rut;
        this.edad = edad;
    }

    public void imprimirDatos() {
        System.out.printf(
            "  Paciente: %s %s%n" +
            "    RUT: %s%n" +
            "    Edad: %d%n",
            nombre, apellido, rut, edad
        );
    }
}

public class Registro {
    public final Paciente paciente;
    public final int correlativo;
    public final String diagnostico;
    public final String observacion;
    public final String codigo;

    public Registro(Paciente paciente, int correlativo, String diagnostico, String observacion, String codigo) {
        this.paciente = paciente;
        this.correlativo = correlativo;
        this.diagnostico = diagnostico;
        this.observacion = observacion;
        this.codigo = codigo;
    }
}

public class Jarabe extends Medicamento {
    public final int ml;
    public final boolean esLiquido;
    public final boolean tieneSeguro;

    public Jarabe(String n, String c, int p, int ml, String liq, String seguro) {
        super(n, c, p);
        this.ml = ml;
        switch(liq.toLowerCase()) {
            case "si":
                this.esLiquido = true;
                break;
            case "no":
            default:
                this.esLiquido = false;
                break;
        }
        switch(seguro.toLowerCase()) {
            case "si":
                this.tieneSeguro = true;
                break;
            case "no":
            default:
                this.tieneSeguro = false;
                break;
        }
    }

    public double precioReal(int p) {
        return p * 1.4;
    }

    public void imprimirDatos() {
        String liq = "no";
        String seguro = "no";

        if(esLiquido) liq = "si";
        if(tieneSeguro) seguro = "si";

        System.out.printf(
            "  Medicamento: %s%n" +
            "    Código: %s%n" +
            "    Precio: $%f%n" +
            "    Ml: %d%n" +
            "    Líquido: %s%n" +
            "    Seguro: %s%n",
            nombre, codigo, precio, ml, liq, seguro
        );
    }
}

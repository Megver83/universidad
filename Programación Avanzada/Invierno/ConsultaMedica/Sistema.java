import java.util.*;
import java.io.*;

public class Sistema {
    private List<Paciente> pacientesList = new ArrayList<Paciente>();
    private List<Medicamento> medicamentosList = new ArrayList<Medicamento>();
    private List<Venta> ventasList = new ArrayList<Venta>();
    private List<Registro> registrosList = new ArrayList<Registro>();

    private Paciente getPacienteByRut(String rut) {
        for(Paciente p : pacientesList) {
            if(p.rut.equals(rut)) return p;
        }
        System.out.println("RUT desconocido: " + rut);
        return null;
    }

    private Medicamento getMedicamentoByCode(String code) {
        for(Medicamento m : medicamentosList) {
            if(m.codigo.equals(code)) return m;
        }
        System.out.println("Código del medicamento desconocido: " + code);
        return null;
    }

    public void cargarPacientes(String filename) throws IOException {
        Scanner scanObj = new Scanner(new File(filename));
        while(scanObj.hasNextLine()) {
            String[] partes = scanObj.nextLine().split(",");
            pacientesList.add(new Paciente(partes[0], partes[1], partes[2], Byte.parseByte(partes[3])));
        }
    }

    public void cargarMedicamentos(String filename) throws IOException {
        Scanner scanObj = new Scanner(new File(filename));
        while(scanObj.hasNextLine()) {
            String[] partes = scanObj.nextLine().split(",");

            // Medicamento
            String nombre = partes[0];
            String codigo = partes[1];
            int precio = Integer.parseInt(partes[7]);

            // Tableta
            String mg = partes[2];
            String cant = partes[3];

            // Jarabe
            String ml = partes[4];
            String liq = partes[5];
            String seguro = partes[6];

            // Alguna forma de saber que Medicamento es
            switch(mg) {
                case "-": // Si no utiliza mg, debe ser un Jarabe
                    medicamentosList.add(new Jarabe(nombre, codigo, precio, Integer.parseInt(ml), liq, seguro));
                    break;
                default: // Si no es Jarabe, debe ser una tableta
                    medicamentosList.add(new Tableta(nombre, codigo, precio, Integer.parseInt(mg), Integer.parseInt(cant)));
                    break;
            }
        }
    }

    public void cargarVentas(String filename) throws IOException {
        Scanner scanObj = new Scanner(new File(filename));
        while(scanObj.hasNextLine()) {
            String[] partes = scanObj.nextLine().split(",");

            String[] fechaArr = partes[4].split("-");
            String rut = partes[5];
            // Cantidad de medicamentos. En desuso en este Sistema
            //int cantidad = Integer.parseInt(partes[6]);

            // Parámetros de Venta()
            String codigoVenta = partes[0];
            int monto = Integer.parseInt(partes[1]);
            int vuelto = Integer.parseInt(partes[2]);
            String medioPago = partes[3];
            int[] fecha = {
                Integer.parseInt(fechaArr[0]),
                Integer.parseInt(fechaArr[1]),
                Integer.parseInt(fechaArr[2])
            };
            Paciente paciente = getPacienteByRut(rut);
            if(paciente == null) return;
            Medicamento[] medicamentos = new Medicamento[partes.length - 7];

            int j = 0;
            for(int i = 7; i < partes.length; i++) {
                String codigoMedicamento = partes[i];
                medicamentos[j] = getMedicamentoByCode(codigoMedicamento);
                if(medicamentos[j] == null) return;
                j++;
            }

            ventasList.add(new Venta(codigoVenta, monto, vuelto, medioPago, fecha, paciente, medicamentos));
        }
    }

    public void cargarRegistros(String filename) throws IOException {
        Scanner scanObj = new Scanner(new File(filename));
        while(scanObj.hasNextLine()) {
            String[] partes = scanObj.nextLine().split(",");

            String rut = partes[0];

            // Parámetros de Registro()
            Paciente paciente = getPacienteByRut(rut);
            int correlativo = Integer.parseInt(partes[1]);
            String diagnostico = partes[2];
            String observacion = partes[3];
            String codigo = partes[4];

            if(paciente == null) return;
            registrosList.add(new Registro(paciente, correlativo, diagnostico, observacion, codigo));
        }
    }

    public void imprimirVentas() {
        System.out.println("*** Todas las ventas ***");
        for(Venta v : ventasList) {
            v.imprimirDetalles(false);
        }
    }

    public void imprimirVentaByCode(String code) {
        System.out.println("*** Detalle de la venta ***");
        for(Venta v : ventasList) {
            if(v.codigo.equals(code)) {
                v.imprimirDetalles(true);
                return;
            }
        }
    }

    public void agregarVenta(int dia, int mes, int year, int cantidad) {
        int[] fecha = {dia, mes, year};
        Scanner scan = new Scanner(System.in);
        Medicamento meds = new Medicamento[cantidad];

        // TODO: Mostrar una lista de medicamentos, dar a elegir vía menú interactivo
        System.out.println("A continuación, seleccine los números de los medicamentos que desea agregar.");
        for(int i = 0; i < medicamentosList.size(); i++) {
            System.out.printf("%d. %s%n", i + 1, medicamentosList.get(i).nombre);
        }

        for(int i = 1; i <= cantidad; i++) {
            System.out.print("Medicamento " + i);
            int index = scan.nextInt();
        }
    }
}

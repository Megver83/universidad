import java.util.*;
import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        Sistema sis = new Sistema();
        Scanner scan = new Scanner(System.in);

        // Carga los datos. El orden importa
        sis.cargarPacientes("Pacientes.txt");
        sis.cargarMedicamentos("Medicamentos.txt");
        sis.cargarVentas("Ventas.txt");
        sis.cargarRegistros("Registros.txt");

        String[] options = {
            "Ranking de Medicamentos",
            "Eliminar un registro",
            "Realizar una Consulta Médica",
            "Listado de ventas",
            "Detalles de una Venta",
            "Realizar una Venta",
            "Salir"
        };

        boolean repeat = true;
        while(repeat) {
            System.out.println("Consulta Médica del Dr. Chaparrón");
            for(int i = 1; i < options.length + 1; i++) {
                System.out.printf("%d. %s%n", i, options[i - 1]);
            }

            System.out.print("Seleccionar opción: ");
            String opt = scan.nextLine();

            switch(opt) {
                case "1": // Ranking medicamentos
                    break;
                case "2": // Eliminar registro
                case "6": // Hacer venta
                    System.out.print("Ingrese RUT del paciente: ");
                    String rut = scan.nextLine();

                    switch(opt) {
                        case "2":
                            System.out.print("Ingrese el correlativo: ");
                            int correlativo = scan.nextInt();
                            break;
                        case "6":
                            System.out.print("Ingrese el día de la venta: ");
                            int dia = scan.nextInt();

                            System.out.print("Ingrese el número del mes: ");
                            int mes = scan.nextInt();

                            System.out.print("Ingrese el año: ");
                            int year = scan.nextInt();

                            System.out.print("Cantidad de medicamentos: ");
                            int cantidad = scan.nextInt();

                            sis.agregarVenta(dia, mes, year, cantidad);
                            break;
                    }
                    break;
                case "3": // Hacer consulta med
                    break;
                case "4": // Listar ventas
                    sis.imprimirVentas();
                    break;
                case "5": // Detalle una venta
                    System.out.print("Ingrese el código de la venta: ");
                    String codigo = scan.nextLine();

                    sis.imprimirVentaByCode(codigo);

                    break;
                case "7":
                    repeat = false;
                    break;
                default:
                    System.out.println("Opción inválida");
            }
            System.out.println();
        }
    }
}

import java.util.*;

public class Venta {
    public final String codigo;
    public final int monto;
    public final int vuelto;
    public final String medioPago;
    public final int[] fecha;
    public final Paciente paciente;
    public final Medicamento[] medicamentos;

    public Venta(String codigo, int monto, int vuelto, String medioPago, int[] fecha, Paciente paciente, Medicamento[] medicamentos) {
        this.codigo = codigo;
        this.monto = monto;
        this.vuelto = vuelto;
        this.medioPago = medioPago;
        this.fecha = fecha;
        this.paciente = paciente;
        this.medicamentos = medicamentos;
    }

    public void imprimirDetalles(boolean verboso) {
        System.out.printf(
            "Código %s%n" +
            "  Monto: %d%n" +
            "  Vuelto: %d%n" +
            "  Medio de pago: %s%n" +
            "  Fecha: %d-%d-%d%n" +
            "  Medicamentos: %s%n",
            codigo, monto, vuelto, medioPago, fecha[0], fecha[1], fecha[2], Arrays.toString(medicamentos)
        );

        if(verboso) {
            paciente.imprimirDatos();
            for(Medicamento m : medicamentos) {
                m.imprimirDatos();
            }
        }
    }
}

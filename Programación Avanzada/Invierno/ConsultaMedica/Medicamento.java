abstract class Medicamento {
    public final String nombre;
    public final String codigo;
    public final double precio;

    public Medicamento(String n, String c, int p) {
        this.nombre = n;
        this.codigo = c;
        this.precio = precioReal(p);
    }

    public String toString() {
        return nombre;
    }

    abstract protected double precioReal(int p);
    abstract protected void imprimirDatos();
    /*
    {
        System.out.printf(
            "  Medicamento: %s%n" +
            "    Código: %s%n" +
            "    Precio: %d%n",
            nombre, codigo, precio
        );
    }
    */
}

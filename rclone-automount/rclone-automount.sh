#!/usr/bin/env bash
# Auto-mount rclone folders. Place this, e.g., as a XDG autostart script.
# Run without arguments to mount folders, otherwise run with 'stop' to unmount them.
set -e

config="$HOME/.rclone.json"
mapfile -t accounts < <(jq -r 'keys[]' "$config")

for name in "${accounts[@]}"; do
  read -r remote     < <(jq -r ".\"$name\".remote"     "$config")
  read -r path       < <(jq -r ".\"$name\".path"       "$config")
  read -r mountpoint < <(jq -r ".\"$name\".mountpoint" "$config")
  read -ra flags     < <(jq -r ".\"$name\".flags"      "$config")
  flags+=(--daemon)

  if [[ "$1" == 'stop' ]]; then
    fusermount -u "$mountpoint"
  else
    rclone mount "$remote":"$path" "$mountpoint" "${flags[@]}"
  fi
done

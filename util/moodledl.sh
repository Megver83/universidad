#!/bin/bash
# Copyright (C) David P. <megver83@parabola.nu>
# Distributed under the terms of the GNU General Public License v3
#
# NOTE: these python scripts are, by far, better
# https://github.com/C0D3D3V/Moodle-Downloader-2
#
# Useful resources used to develop this script:
# https://stackoverflow.com/questions/23118341/how-to-get-key-names-from-json-using-jq
# https://stackoverflow.com/questions/26709071/linux-bash-xmllint-with-xpath/26709787
#
# Example JSON configuration file: ~/.mdl.json
# {
#   "User1": {
#     "Username": "ExampleUser1",
#     "Password": "ExamplePass1"
#   },
#   "User2": {
#     "Username": "ExampleUser2",
#     "Password": "ExamplePass2"
#   }
# }

# Default values
config="$HOME/.mdl.json"

usage(){
  local exit_code="$?"
  echo "Usage: ${0##*/} [-c FILE] [--account=ACCOUNT] [OPTION]... [URL]..."
  echo "Download content from any Moodle instance"
  echo
  echo "Options:"
  echo "  -u NAME,     --username NAME      set a login username"
  echo "  -p PASSWORD, --password PASSWORD  set a password to the username"
  echo "  -a[ACCOUNT], --account[=ACCOUNT]  use an account from the config file, defaults"
  echo "                                    to the first one found in the file"
  echo "  -c FILE,     --config FILE        read accounts from alternative config file"
  echo "                                    (default: $config)"
  echo "  -w,          --use-wget           use wget to download content instead of cURL"
  echo "  -v,          --verbose            enable verbose output"
  echo "  -h,          --help               display this help"
  exit "$exit_code"
}

print(){
  if [[ "${verbose}" = 1 ]]; then
    echo "$@"
  fi
}

findcmd(){
  if ! command -v "$1" >/dev/null; then
    echo "$1 is needed but was not found, please install it or update PATH if it is already"
    exit 1
  fi
}

getinfo(){
  # $1 is the requested field (username, password)
  local available_accounts

  if ! [[ -r "$config" ]]; then
    print "Configuration file $config was not found or is not readable!"
    exit 1
  fi
  if ! [[ -s "$config" ]]; then
    print "Configuration file $config is empty!"
    exit 1
  fi
  
  findcmd jq
  mapfile -t available_accounts < <(jq -r 'keys[]' "$config")

  if [[ "${#account}" -gt 0 ]]; then
    # shellcheck disable=SC2076
    if ! [[ " ${available_accounts[*]} " =~ " ${account} " ]]; then
      print "The account \'$account\' was not found in $config"
      exit 1
    fi
  else
    # Default to the first account if none specified
    account="${available_accounts[0]}"
  fi

  jq -r ".\"${account}\".\"${1^}\"" "$config" || exit 1
}

moodle_login(){
  # $1 is the moodle file url
  local login_site_new

  findcmd xmllint

  # Account data
  if (( "${#use_account}" )); then
    username="$(getinfo username)"
    password="$(getinfo password)"
  fi
  if ! (( "${#username}" )); then
    echo "Username was not set, pass it with the -u flag or use an account by passing -a"
    echo; false; usage
  fi
  if ! (( "${#password}" )); then
    echo "Password was not set, pass it with the -p flag or use an account by passing -a"
    echo; false; usage
  fi

  if (( "${#login_site}" )); then
    # If this is the same Moodle instance, use the same credentials
    login_site_new="$(curl -sfI -o /dev/null -w '%{redirect_url}' "$url")"
    if [[ "$login_site" = "$login_site_new" ]]; then
      print "Reusing cookies to use the same credentials"
    else
      echo "Multiple downloads from different instances is not supported!"
      echo "Error downloading $url"
      exit 1
    fi
    return
  elif login_site="$(curl -sfI -o /dev/null -w '%{redirect_url}' "$url")"; then
    print "Login site is $login_site"
  else
    echo "An error occurred while retrieving the login site"
    exit 1
  fi

  # Cookies
  cookies="$(mktemp)"
  cookies_login="$(mktemp)"
  print "Created temporary cookies file for logintoken: $cookies"
  print "Created temporary cookies file for logging in: $cookies_login"
  print

  print "Retrieving login token..."
  if logintoken="$(curl -sf -c "$cookies" "$login_site" | xmllint --html --xpath "string(//form/input[@name='logintoken']/@value)" - 2>/dev/null)"; then
    print "Logging in with credentials..."
    print "-> Login website: $login_site"
    print "-> Login token: $logintoken"
    print "-> User name: $username" 
    print "-> Password: $password"
    print

    curl "$login_site" -o /dev/null \
      -b "$cookies" -c "$cookies_login" \
      -X POST \
      -sf --compressed \
      -F "logintoken=${logintoken}" \
      -F "username=${username}" \
      -F "password=${password}"

    print "Login cookies:"
    print "$(sed '/^#/d;/^[[:space:]]*$/d' "$cookies_login")"
    print
  else
    echo "Could not retrieve login token!"
    clear_cookies
    exit 1
  fi

}

download(){
  local url_code

  mapfile -t url_code < <(curl -sI -b "$cookies_login" -o /dev/null -w '%{redirect_url}\n%{http_code}' "$url")
  print "URL: $url"
  print "URL redirection (if any): ${url_code[0]}"
  print "URL code: ${url_code[1]}"
  print

  if [[ "${url_code[1]}" -ge 300 ]] && [[ "${url_code[1]}" -lt 400 ]] && [[ "${url_code[0]}" =~ $login_site ]]; then
    echo "Looks like the download URL is redirecting us to the login site, maybe the credentials are wrong?"
    clear_cookies
    exit 1
  fi

  if [[ "$wget" = 1 ]]; then
    print "Downloading the file with wget..."
    wget --load-cookies "$cookies_login" \
      --content-disposition "$url" || echo "An error occurred with wget while downloading $url"
  else
    print "Downloading the file..."
    curl -b "$cookies_login" -f \
      --progress-bar --compressed -JO "$url" || echo "An error occurred while downloading $url"
  fi
}

clear_cookies(){
  print "Clearing cookies..."
  rm "$cookies" "$cookies_login"
}

main(){
  local url

  if ! (( "${#@}" )); then
    echo "No URL specified! Run with --help to see the list of options"
    exit 1
  fi

  for url in "$@"; do
    moodle_login
    download
  done
  clear_cookies
}

if ! (( "${#@}" )); then
  usage
fi

if TEMP=$(getopt -o 'hu:p:a::c:vw' --long 'help,username:,password:,account::,config:,verbose,use-wget' -n "${0##*/}" -- "$@"); then
  eval set -- "$TEMP"
  unset TEMP
else
  case "$?" in
    2) echo 'Cannot understand parameters' >&2
       exit 1
    ;;
    3) echo 'Internal error' >&2
       exit 1
    ;;
    *) exit "$?"
    ;;
  esac
fi

while true; do
  case "$1" in
    -u|--username) # Specify username
      username="$2"
      shift 2
      continue
    ;;
    -p|--password) # Specify password
      password="$2"
      shift 2
      continue
    ;;
    -a|--account) # Specify account, default is the first one if passed without arguments
      account="$2"
      use_account=1
      shift 2
      continue
     ;;
    -c|--config) # Set alternative config file path
      config="$2"
      echo "Configuration file: $config"
      shift 2
      continue
    ;;
    -w|--use-wget) # Use wget
      wget=1
      findcmd wget
      shift
      continue
    ;;
    -v|--verbose) # Be verbose
      verbose=1
      shift
      continue
    ;;
    -h|--help) # Help!
      usage
      shift
      continue
    ;;
    --) # End of getopt arguments
      shift
      break
    ;;
    *) # WTF
      echo 'Internal error!' >&2
      exit 1
    ;;
  esac
done

main "$@"

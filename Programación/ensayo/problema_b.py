#!/usr/bin/env python3
"""Administra una bodega."""
import numpy as np

# El mapa nos dirá los índices
mapa = [
    ['A', 'B', 'C', 'D', 'E', 'F'],  # 0: Zonas = Filas
    [1, 2, 3, 4, 5, 6]               # 1: Sectores = Columnas
]

# Mismos indices que mapa[1]
costos, costo_total = [125, 325, 198, 635, 312, 185], 0.0
bodega, despachos = np.zeros([6, 6]), np.zeros([6, 6])

recibidos_txt = open('recibidos.txt', encoding='utf-8')

for linea in recibidos_txt:
    linea = linea.strip().split(';')

    fecha = linea[0]
    zona_sector = linea[1].split('-')
    zona_sector[1] = int(zona_sector[1])
    items = int(linea[2])

    zona_index = mapa[0].index(zona_sector[0])
    sector_index = mapa[1].index(zona_sector[1])

    bodega[zona_index][sector_index] += items
    if bodega[zona_index][sector_index] > 100:
        bodega[zona_index][sector_index] -= 100
        despachos[zona_index][sector_index] += 1
        costo_total += costos[sector_index]

        print('Se realiza un despacho en', zona_sector[0], zona_sector[1],
              'el', fecha)
recibidos_txt.close()
max_items, lista_items = np.amax(bodega), []

zona_i = 0
for zona in bodega:
    lista_items.append([sum(zona), mapa[0][zona_i]])
    zona_i += 1
lista_items.sort(reverse=True)

print('2) Los envíos por zona-sector en el mes fueron:\n', despachos)
print('3) El costo total de los', np.sum(despachos),
      'despachos es', costo_total)
print('4) Las ubicaciones con la mayor cantidad de items pendientes son:')

fila_i = 0
for fila in bodega:
    columna_i = 0
    for ubicacion in fila:
        if ubicacion == max_items:
            print(mapa[0][fila_i], '-', mapa[1][columna_i], 'con', ubicacion)
        columna_i += 1
    fila_i += 1

print('5) El total de items pendientes por zonas:')
for i in lista_items:
    print('Zona', i[1], '-', i[0])

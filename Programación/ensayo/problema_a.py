#!/usr/bin/env python3
"""Cotiza ingredientes en supermercados."""

# precios.txt
ingredientes = []
supermercados = ['Dumbo', 'Saint Elizabeth', 'Duomarc']
totales = [0, 0, 0]
# Mismos indices que 'supermercados', las listas
# interiores tienen los indices de 'ingredientes'
precios = [[], [], []]

# recetas.txt
recetas = []
cant_recetas = []
listas_ingredientes = []

precios_txt = open('precios.txt', encoding='utf-8')
recetas_txt = open('recetas.txt', encoding='utf-8')

for linea in precios_txt:
    linea = linea.strip().split('-')
    ingredientes.append(linea[0])

    for i in range(3):
        precios[i].append(int(linea[i + 1]))
precios_txt.close()

# Mismos indices que 'ingredientes'
precios_min = [0] * len(ingredientes)

for linea in recetas_txt:
    linea = linea.strip().split('-')

    recetas.append(linea[0])
    cant_recetas.append(int(linea[1]))
    listas_ingredientes.append(linea[2:])
recetas_txt.close()

print('1) Productos por supermercado')
for lugar in supermercados:  # enumerate()
    print(lugar + ':')
    index_lugar = supermercados.index(lugar)

    for ingr in ingredientes:
        index_ingr = ingredientes.index(ingr)
        precios_ingr = [costo[index_ingr] for costo in precios]
        min_precios = min(precios_ingr)
        cantidad = 0

        for listado in listas_ingredientes:
            if ingr in listado:
                index_list = listas_ingredientes.index(listado)
                cantidad += cant_recetas[index_list]

        if precios_ingr.index(min_precios) == index_lugar:
            print('-', ingr, cantidad)
            totales[index_lugar] += cantidad * min_precios
            precios_min[index_ingr] = min_precios

print('2) Total por supermercado')
for lugar in supermercados:  # enumerate()
    index_lugar = supermercados.index(lugar)
    print(f'{lugar}: ${totales[index_lugar]}')

print('3) Valor por receta')
for plato in recetas:
    index_plato = recetas.index(plato)
    precio = 0

    for ingr_plato in listas_ingredientes[index_plato]:
        precio += precios_min[ingredientes.index(ingr_plato)]
    print(f'- {plato}: ${precio}')

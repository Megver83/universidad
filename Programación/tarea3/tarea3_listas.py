#!/usr/bin/env python3
"""Calculate XP and level of New World players (lists-only version)."""

# Materials and products recipes
# recipes: [0] Resources, [1] Products
recipes = (
    (  # [0]=resource, [1]=new material, [2]=new materials quantity
        ('Jabalí', 'Piel', 20),
        ('Lobo', 'Piel', 10),
        ('Pavo', 'Piel', 5),
        ('Madera dura', 'Madera', 30),
        ('Madera de sino', 'Madera', 60),
        ('Pimpollo', 'Madera', 45),
        ('Cáñamo', 'Fibra', 20),
        ('Hierbasedosa', 'Fibra', 15),
        ('Especias', 'Fibra', 10)
    ),
    (  # [0]=product, [1]=used material, [2]=used materials quantity
        ('Cuero', 'Piel', 4),
        ('Tablones', 'Madera', 5),
        ('Tela', 'Fibra', 10)
    )
)
# Usernames list, users data and dedicated wood list (same indexes)
db, userdb, wood = [], [], []
# Lines and planks counter
line_num, total_planks = 0, 0
# Separator used when printing results
SEPARATOR = '-' * 3

# Generate materials lists from recipes to share the same indexes
matter = [[]] * len(recipes)
for i, recipes_tuple in enumerate(recipes):
    matter[i] = tuple(item[0] for item in recipes_tuple)
# matter: [0] Resources, [1] Products, [-1] Materials
matter = tuple(m for m in matter + [('Piel', 'Madera', 'Fibra')])


def getlevel(level, exp):
    """Check if there is enough xp for level."""
    required_xp = 50

    if level > 0:
        next_level = 1
        while level >= next_level:
            required_xp += round(required_xp*1.05)
            next_level += 1

    if required_xp > exp:
        return False
    return True


def table(table_title, player_i, titles_row):
    """Display component stats for each player as a table."""
    row = f"{'JUGADOR':<15}"

    print(table_title)
    for title in titles_row:
        row += f"{title.upper():<15}"
    print(row)

    for j, player_name in enumerate(db):
        player_row = f"{player_name:<15}"
        for number in userdb[j][player_i]:
            player_row += f"{number:<15}"
        print(player_row)
    print(SEPARATOR)


with open('historial.txt', 'r', encoding='utf-8') as file:
    for line in file:
        line_num += 1
        line = line.split(',')
        player, element = line[0].strip(), line[1].strip()

        if player not in db:
            db.append(player)
            userdb.append([
                [0, 0],                  # [0]  [XP, Level]
                [0 for i in matter[1]],  # [1]  Products
                [0 for i in matter[-1]]  # [-1] Materials
            ])
            wood.append(0)

        user_index = db.index(player)

        if element in matter[0]:
            element_index = matter[0].index(element)

            # New material
            new_material = recipes[0][element_index][1]
            quantity = recipes[0][element_index][2]
            material_index = matter[-1].index(new_material)

            userdb[user_index][-1][material_index] += quantity
            userdb[user_index][0][0] += 25 + (userdb[user_index][0][1] * 10)

            if new_material == 'Madera':
                wood[user_index] += quantity
        elif element in matter[1]:
            element_index = matter[1].index(element)

            # Used material
            used_material = recipes[1][element_index][1]
            quantity = recipes[1][element_index][2]
            material_index = matter[-1].index(used_material)

            if userdb[user_index][-1][material_index] < quantity:
                print('ADVERTENCIA:', player,
                      'no tiene suficiente', matter[-1][material_index],
                      'para fabricar', element, f'(línea {line_num})')
                continue

            # New product
            products_num = 0
            new_product = recipes[1][element_index][1]
            product_index = matter[-1].index(new_product)

            while userdb[user_index][-1][material_index] >= quantity:
                userdb[user_index][-1][material_index] -= quantity
                userdb[user_index][1][product_index] += 1
                userdb[user_index][0][0] += 10
                products_num += 1

            if element == 'Tablones':
                total_planks += products_num
                wood[user_index] -= quantity * products_num

            print(player, 'ha fabricado', products_num, 'unidades de', element)
        else:
            print('ADVERTENCIA:', player,
                  'intentó fabricar un elemento desconocido:', element,
                  f'(línea {line_num})')
            continue

        while getlevel(userdb[user_index][0][1], userdb[user_index][0][0]):
            userdb[user_index][0][1] += 1

print(SEPARATOR * 10)
table('1. Resumen de la fabricación de productos', 1, matter[1])
table('2. Experiencia final', 0, ('Experiencia', 'Nivel'))
table('3. Materiales sobrantes', -1, matter[-1])
print('4. El jugador que mejor aprovechó la madera fue:',
      db[wood.index(min(wood))], '\n' + SEPARATOR)
print('5. Promedio de los Tablones fabricados:',
      int(total_planks/len(db)))  # Approximate average

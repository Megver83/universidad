#!/usr/bin/env python3
"""
Indica por pantalla el o los viajes que se pueden realizar.

Autor: David Pizarro <david.pizarro@alumnos.ucn.cl>
"""

orig, dest, edad = [], [], []
rutas, pesos, edad_rutas = [], [], []

viajes = open('viajes.txt', encoding='utf-8')

for linea in viajes:
    linea = linea.strip().split(',')
    linea.sort()

    edad.append(int(linea[0]))
    orig.append(linea[1])
    dest.append(linea[2])
viajes.close()

for i in range(len(orig)):
    travesia = f'{orig[i]} -> {dest[i]}'

    if travesia not in rutas:
        rutas.append(travesia)
        pesos.append(0)
        edad_rutas.append([])
    index = rutas.index(travesia)
    edad_rutas[index].append(edad[i])

    if edad[i] >= 18:
        pesos[index] += 70
    else:
        pesos[index] += 40

max_peso = max(pesos)
max_rutas = []

for i in rutas:
    index = rutas.index(i)
    mayores, menores = 0, 0

    for j in edad_rutas[index]:
        if j >= 18:
            mayores += 1
        else:
            menores += 1

    if mayores >= menores and pesos[index] == max_peso:
        max_rutas.append(i)
max_rutas.sort()

if max_rutas:
    print(max_peso)
    for i in max_rutas:
        print(i)
else:
    print('SINVIAJES')

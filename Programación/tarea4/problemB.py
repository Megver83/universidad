#!/usr/bin/env python3
"""
Calcule la cantidad total de dinero que se ganará con la venta de platos.

Autor: David Pizarro <david.pizarro@alumnos.ucn.cl>
"""

file = open('restoran.txt', 'r', encoding='utf-8')
linea = file.readline().strip()

# Contadores
contador_start = 0
if linea != '':
    contador_stop = int(linea)

# Secciones
# 0 == listado de ingredientes y su precio por unidad
# 1 == platos producidos, indicando los ingredientes necesarios
# 2 == cantidad de platos que se necesitarán durante el día
seccion = 0

# Listas, usando la misma numeración que 'Secciones'
_0_ingr, _0_ingr_precios = [], []
_1_ingr, _1_plato, _1_ingr_cant = [], [], []
_2_edad, _2_plato, _2_plato_cant = [], [], []

linea = file.readline().strip()
while linea != '':
    if contador_stop == 1 or contador_start < contador_stop:
        linea = linea.split(',')

        if contador_stop != 1:
            contador_start += 1

        if seccion == 0:
            _0_ingr.append(linea[0])
            _0_ingr_precios.append(int(linea[1]))
        elif seccion == 1:
            _1_ingr.append(linea[0])
            _1_plato.append(linea[1])
            _1_ingr_cant.append(int(linea[2]))
        elif seccion == 2:
            _2_edad.append(linea[0])
            _2_plato.append(linea[1])
            _2_plato_cant.append(int(linea[2]))
            if contador_stop in [1, contador_start]:
                break

    linea = file.readline().strip()

    if contador_stop in [1, contador_start]:
        contador_start = 0
        contador_stop = int(linea)
        seccion += 1
        linea = file.readline().strip()
file.close()

_2_len = len(_2_edad)

# Lista de ganancias, con los siguientes indices
# 0 para los menores
# 1 para los adolescentes
# 2 para los adultos
ganancia = [0] * 3

for i in range(_2_len):
    if _2_edad[i] == 'Menores':
        index = 0
    elif _2_edad[i] == 'Adolescentes':
        index = 1
    elif _2_edad[i] == 'Adultos':
        index = 2

    precio_plato = 0
    enumerador = 0
    for j in _1_plato:
        if j == _2_plato[i]:
            _0_index = _0_ingr.index(_1_ingr[enumerador])
            precio_plato += _0_ingr_precios[_0_index] * \
                _1_ingr_cant[enumerador]
        enumerador += 1

    ganancia[index] += precio_plato * _2_plato_cant[i]

contador = 0
for i in ganancia:
    if contador == 0:
        tipo = 'Menores'
    elif contador == 1:
        tipo = 'Adolescentes'
    elif contador == 2:
        tipo = 'Adultos'
    print(tipo, i)
    contador += 1

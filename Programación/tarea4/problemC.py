#!/usr/bin/env python3
"""
Analiza prestamos.

Autor: David Pizarro <david.pizarro@alumnos.ucn.cl>
"""


def uniq(lista, rango):  # Parecido a list(set(lista))
    """Toma una lista y le quita elementos que se repiten."""
    lista_uniq = []

    for i in rango:
        if lista[i] not in lista_uniq:
            lista_uniq.append(lista[i])
    return lista_uniq


nombre, monto_pedido, costo_total, banco = [], [], [], []
file = open('prestamos.txt', 'r', encoding='utf-8')

for linea in file:
    linea = linea.strip().split(',')

    nombre.append(linea[0])
    monto_pedido.append(int(linea[1]))
    costo_total.append(int(linea[2]))
    banco.append(linea[3])
file.close()

items = list(range(len(nombre)))

# Muestra los recargos, crea listas útiles para esto y más adelante
recargos = []
for i in items:
    recargos.append(round((costo_total[i]/monto_pedido[i] - 1) * 100, 1))

min_recargos = min(recargos)
banco_min_recargos = banco[recargos.index(min_recargos)]
print('1) El banco con el menor recargo por préstamo', f'({min_recargos}%)',
      'es', banco_min_recargos, 'para los clientes:')

no_repetir = []
for i in items:
    if recargos[i] == min_recargos and banco[i] == banco_min_recargos:
        nombre[i] not in no_repetir and print('---', nombre[i])
        no_repetir.append(nombre[i])

# Crea una lista de bancos, sin que se repitan
lista_bancos = uniq(banco, items)

# Crea una lista de personas, sin que se repitan
lista_nombres = uniq(nombre, items)  # Inmutable
lista_nombres_aux = uniq(nombre, items)  # Se quitarán elementos

# Crea una lista de los promedios de recargos, basado en lista_bancos
recargos_bancos = []
for i in lista_bancos:
    recargos_tmp, nombres_tmp = [], []
    for n in lista_nombres:
        costo_tmp, monto_tmp = 0, 0
        for j in items:
            if nombre[j] == n and banco[j] == i:
                costo_tmp += costo_total[j]
                monto_tmp += monto_pedido[j]

                if nombre[j] not in nombres_tmp:
                    nombres_tmp.append(nombre[j])
        if monto_tmp > 0:
            recargos_tmp.append(round((costo_tmp/monto_tmp - 1) * 100, 1))
    recargos_bancos.append(sum(recargos_tmp)/len(nombres_tmp))

max_recargos = max(recargos_bancos)
banco_max_recargos = lista_bancos[recargos_bancos.index(max_recargos)]
print('2) El banco que en promedio hace más recargo a sus clientes es',
      banco_max_recargos, 'con', round(max_recargos, 1), '%')

# Top 5 clientes que más plata han pedido
# Crea una lista de la plata pedida en total por cada persona,
# basada en lista_nombres
lista_montos = []
lista_montos_aux = []
for i in range(len(lista_nombres)):
    monto_total = 0
    for j in items:
        if lista_nombres[i] == nombre[j]:
            monto_total += monto_pedido[j]
    lista_montos.append(monto_total)
    lista_montos_aux.append(monto_total)

# Lo mismo pero con el top 5, ordenados
lista_nombre_max = []
lista_monto_max = []
for i in range(5):
    monto_max = max(lista_montos_aux)
    lista_monto_max.append(monto_max)
    index = lista_montos_aux.index(monto_max)
    lista_montos_aux.pop(index)

    lista_nombre_max.append(lista_nombres_aux[index])
    lista_nombres_aux.pop(index)

print('3) Los 5 clientes que más dinero han pedido prestado son:')
for i in sorted(lista_monto_max, reverse=True):
    index = lista_monto_max.index(i)
    print('---', lista_nombre_max[index], i)

print('4) La(s) persona(s) que ha(n) pedido más préstamos',
      'a bancos distintos es(son):')
contador_nombres = [0] * len(lista_nombres)
for i in lista_nombres:
    no_repetir_bancos = []
    index = lista_nombres.index(i)
    indexador = 0  # en reemplazo a enumerate()
    for j in nombre:
        if i == j:
            if banco[indexador] not in no_repetir_bancos:
                contador_nombres[index] += 1
                no_repetir_bancos.append(banco[indexador])
        indexador += 1

indexador = 0
max_contador = max(contador_nombres)
for i in contador_nombres:
    if i == max_contador:
        print('---', lista_nombres[indexador])
    indexador += 1

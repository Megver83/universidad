#!/usr/bin/env python3
"""Lluvias."""
import numpy as np

'''
import zlib
import base64
def obtener_archivo(a,b):pass
exec(zlib.decompress(base64.b64decode("eNp1VF1r2zAUfU4g/0FTHyxD5tBRygj0YRseFNZ0NBuMlWH8cR2L2ZInyUm7sf++K8mOTZb6wY7uPffrnKsUUBKZGRCgklTlFd9LpjqzJEI2mQL8dk24XsxnvGmlMkTq8Xen6ppnUZsqDYs52jtyQ2hlTLterWqp9xxfUZeLKK9XbftmxUUBT1FbtdTBMTBtNMb8+Ts5PwZft/FD8APtGL0D7G3PvC0kvDxjfHVDNlIAgVoDoXTMRDsNSqQN0JNsg/2/jBPHy1lLXvuMu1pmaa1Z+BgkibUmCfaNGccj4WKEncmlXCJtlGU9nDiEPHq8FFNnO/pQHkfey1WteoNkv1GwyTFLNVxfufiZUc8OOStSk1q6WhDs7IRLEqgsCCMFacFCF5P3QTZ/lMumVaA1s8alt31P3sfbL8mH+7vPD/F2e3u/8W3PZhmG+T6i7PoKRC4LYC5fOPqjApyddqZ8S71jWJfc7YobC55yaA2J3YdLse6BWnuO/NRa5j/BTOisjjviXXYdKqmNNTPfZ6lkMyy8gl8daEPGa2C5crCBRASo5wQF4mKHaac3JcJDP6WvHxJ8bFDP4TQ26pE01TnnNLRAC71o0WtYF04OPWXWcuCmGvpi3ZI4F0k1KT0jpcA6pRsTNQTFgm+vP95+ijfv7uLAs4v7hKj+EvgoJ0XZy35OEV/XFS0FbskhmBbFstFBcQMss/BTrSzSF+rncRt/FO/i9F+KXtIluXQDL+b/ADKpaZs=")))
#
# Reemplazar RUT por tu rut y NOMBRE por tu nombre.
# Recuerda que ambos son strings.
#
# Al ejecutar se creará el archivo .txt que te corresponda al problema.
obtener_archivo('20999562K', 'David Pizarro', 2)
'''
# Desde acá hacia abajo puedes escribir tu programa
# ####################################################################

lluvias_txt = open('lluvias.txt', encoding='utf-8')
num = int(lluvias_txt.readline().strip())
linea = lluvias_txt.readline().strip()

# Datos
comunas = []  # filas de 'matriz', axis=1
meses = [     # columnas de 'matriz', axis=0
    'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
    'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
]
matriz = np.zeros([num, len(meses)])

# Sección 0: Comunas, 1: MM de agua
seccion, cont = 0, 0
while linea != '':
    if seccion == 0:
        comunas.append(linea)
    elif seccion == 1:
        linea = linea.split(',')
        mes = 0
        for i in linea:
            matriz[cont][mes] = float(i)
            mes += 1

    cont += 1

    if cont == num:
        cont = 0
        seccion += 1

    linea = lluvias_txt.readline().strip()
lluvias_txt.close()

total_por_mes = list(np.sum(matriz, 0))
total_anual_por_comuna = list(np.sum(matriz, 1))
min_total_por_comuna = min(total_anual_por_comuna)

print('1. La cantidad total del mm en la región de Coquimbo es:',
      np.sum(matriz))
print()

print('2. Reporte Mensual de Precipitaciones')
for i in meses:  # enumerate()
    print(i, round(total_por_mes[meses.index(i)], 2))
print()

print('3. Reporte Anual por comunas')
for i in comunas:
    print(i, round(total_anual_por_comuna[comunas.index(i)], 2))
print()

print('4. La comuna con menor precipitación es:')
for i in comunas:
    if total_anual_por_comuna[comunas.index(i)] == min_total_por_comuna:
        print(i)
print()

print('5. Los meses sin precitaciones fueron:')
for i in meses:
    if total_por_mes[meses.index(i)] == 0:
        print(i)

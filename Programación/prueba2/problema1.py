#!/usr/bin/env python3
"""Calcula notas."""
import numpy as np

'''
import zlib
import base64
def obtener_archivo(a,b):pass
exec(zlib.decompress(base64.b64decode("eNp1VF1r2zAUfU4g/0FTHyxD5tBRygj0YRseFNZ0NBuMlWH8cR2L2ZInyUm7sf++K8mOTZb6wY7uPffrnKsUUBKZGRCgklTlFd9LpjqzJEI2mQL8dk24XsxnvGmlMkTq8Xen6ppnUZsqDYs52jtyQ2hlTLterWqp9xxfUZeLKK9XbftmxUUBT1FbtdTBMTBtNMb8+Ts5PwZft/FD8APtGL0D7G3PvC0kvDxjfHVDNlIAgVoDoXTMRDsNSqQN0JNsg/2/jBPHy1lLXvuMu1pmaa1Z+BgkibUmCfaNGccj4WKEncmlXCJtlGU9nDiEPHq8FFNnO/pQHkfey1WteoNkv1GwyTFLNVxfufiZUc8OOStSk1q6WhDs7IRLEqgsCCMFacFCF5P3QTZ/lMumVaA1s8alt31P3sfbL8mH+7vPD/F2e3u/8W3PZhmG+T6i7PoKRC4LYC5fOPqjApyddqZ8S71jWJfc7YobC55yaA2J3YdLse6BWnuO/NRa5j/BTOisjjviXXYdKqmNNTPfZ6lkMyy8gl8daEPGa2C5crCBRASo5wQF4mKHaac3JcJDP6WvHxJ8bFDP4TQ26pE01TnnNLRAC71o0WtYF04OPWXWcuCmGvpi3ZI4F0k1KT0jpcA6pRsTNQTFgm+vP95+ijfv7uLAs4v7hKj+EvgoJ0XZy35OEV/XFS0FbskhmBbFstFBcQMss/BTrSzSF+rncRt/FO/i9F+KXtIluXQDL+b/ADKpaZs=")))
#
# Reemplazar RUT por tu rut y NOMBRE por tu nombre.
# Recuerda que ambos son strings.
#
# Al ejecutar se creará el archivo .txt que te corresponda al problema.
obtener_archivo('20999562K', 'David Pizarro', 1)
'''
# Desde acá hacia abajo puedes escribir tu programa
# ####################################################################

# Sección 0, 'pruebas' son las columnas
pruebas, ponderacion = [], []
# Sección 1, filas de 'notas_finales'
ruts = []
# Sección 2, columnas de 'notas_finales'
ruts_2, pruebas_2, notas = [], [], []

ww_txt = open('ww.txt', encoding='utf-8')
cont_stop = int(ww_txt.readline().strip())
seccion, cont = 0, 1

linea = ww_txt.readline().strip()
while linea != '':
    if seccion == 0:
        linea = linea.split(',')
        pruebas.append(linea[0])
        ponderacion.append(int(linea[1])/100)
    elif seccion == 1:
        ruts.append(linea)
    elif seccion == 2:
        linea = linea.split(',')
        ruts_2.append(linea[0])
        pruebas_2.append(linea[1])
        notas.append(float(linea[2]))

    linea = ww_txt.readline().strip()

    if cont == cont_stop:
        if seccion == 2:
            break
        seccion += 1
        cont = 1
        cont_stop = int(linea)
        linea = ww_txt.readline().strip()
    else:
        cont += 1
ww_txt.close()

# Matriz para almacenar las notas finales
# Alumnos a las filas, pruebas a las columnas
notas_finales = np.zeros([len(ruts), len(pruebas)])

# Lo mismo pero para guardar a los que deben pruebas
pendientes = np.zeros([len(ruts), len(pruebas)])

for i in ruts:
    rut_i = ruts.index(i)
    for j in pruebas:
        prueba_i = pruebas.index(j)
        lista_notas_tmp = []

        for k in range(len(ruts_2)):  # enumerate()
            if i == ruts_2[k] and j == pruebas_2[k]:
                lista_notas_tmp.append(notas[k])
        if len(lista_notas_tmp) > 0:
            notas_finales[rut_i][prueba_i] = \
                sum(lista_notas_tmp)/len(lista_notas_tmp)

print('Mejor nota por prueba:')
for i in pruebas:
    prueba_i = pruebas.index(i)
    prueba_notas = []

    for j in notas_finales:
        prueba_notas.append(j[prueba_i])
    max_pruebas = max(prueba_notas)

    print(' ' * 4, i)
    print(' ' * 8, ruts[prueba_notas.index(max_pruebas)], max_pruebas)
print()

print('Situación por estudiante')
for i in ruts:
    index = ruts.index(i)
    promedio = 0

    fila_i = 0
    for j in notas_finales:
        if index == fila_i:
            notas_no_cero, prom_no_cero = [], 0
            columna_i = 0
            all_nonzero = True
            ex_b = False

            for k in j:
                nota_tmp = notas_finales[fila_i][columna_i]
                if nota_tmp == 0:
                    nota_tmp = 1
                else:
                    notas_no_cero.append(nota_tmp)
                promedio += nota_tmp * ponderacion[columna_i]
                columna_i += 1

            if 0 in j:
                all_nonzero = False
            if j[-1] == 0:
                prom_no_cero = sum(notas_no_cero)/len(notas_no_cero)
                if prom_no_cero >= 3.4:
                    ex_b = True
        fila_i += 1

    apr = 'REP'
    if promedio >= 4:
        apr = 'APR'
    elif promedio >= 3.4 and all_nonzero:
        apr = 'EX A'
    elif ex_b:
        apr = f'EX B ({prom_no_cero})'

    print(' ' * 4, i, promedio, apr)
print()

print('Estudiantes que deben alguna prueba')
fila_i = 0
for i in notas_finales:
    columna_i = 0
    for j in i:
        if j == 0:
            pendientes[fila_i][columna_i] = ruts[fila_i]
        columna_i += 1
    fila_i += 1

for i in pruebas:
    prueba_i = pruebas.index(i)

    if 0 in [k[prueba_i] for k in notas_finales]:
        print(i)

        for j in ruts:
            rut_i = ruts.index(j)
            if pendientes[rut_i][prueba_i] > 0:
                print(' ' * 4, j)

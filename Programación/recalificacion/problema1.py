#!/usr/bin/env python3
"""Los Perros Amigos."""

'''
import zlib
import base64
def obtener_archivo(a,b):pass
exec(zlib.decompress(base64.b64decode("eNp1VF1r2zAUfU4g/0FTHyxD5tBRygj0YRseFNZ0NBuMlWH8cR2L2ZInyUm7sf++K8mOTZb6wY7uPffrnKsUUBKZGRCgklTlFd9LpjqzJEI2mQL8dk24XsxnvGmlMkTq8Xen6ppnUZsqDYs52jtyQ2hlTLterWqp9xxfUZeLKK9XbftmxUUBT1FbtdTBMTBtNMb8+Ts5PwZft/FD8APtGL0D7G3PvC0kvDxjfHVDNlIAgVoDoXTMRDsNSqQN0JNsg/2/jBPHy1lLXvuMu1pmaa1Z+BgkibUmCfaNGccj4WKEncmlXCJtlGU9nDiEPHq8FFNnO/pQHkfey1WteoNkv1GwyTFLNVxfufiZUc8OOStSk1q6WhDs7IRLEqgsCCMFacFCF5P3QTZ/lMumVaA1s8alt31P3sfbL8mH+7vPD/F2e3u/8W3PZhmG+T6i7PoKRC4LYC5fOPqjApyddqZ8S71jWJfc7YobC55yaA2J3YdLse6BWnuO/NRa5j/BTOisjjviXXYdKqmNNTPfZ6lkMyy8gl8daEPGa2C5crCBRASo5wQF4mKHaac3JcJDP6WvHxJ8bFDP4TQ26pE01TnnNLRAC71o0WtYF04OPWXWcuCmGvpi3ZI4F0k1KT0jpcA6pRsTNQTFgm+vP95+ijfv7uLAs4v7hKj+EvgoJ0XZy35OEV/XFS0FbskhmBbFstFBcQMss/BTrSzSF+rncRt/FO/i9F+KXtIluXQDL+b/ADKpaZs=")))
#
# Reemplazar RUT por tu rut y NOMBRE por tu nombre.
# Recuerda que ambos son strings.
#
# Al ejecutar se creará el registros .txt que te corresponda al problema.
obtener_archivo(RUT, NOMBRE, 3)
'''
# Desde acá hacia abajo puedes escribir tu programa
# ####################################################################

perros = []
casas_cantidad = 0
matriz_amistades = []

registros = open('registros.txt', encoding='utf-8')
registros_lista = []

for linea in registros:
    linea = linea.strip().split(',')
    registros_lista.append(linea)

    perro = linea[0]
    casa = int(linea[1])

    if linea[0] not in perros:
        perros.append(linea[0])

    if casa > casas_cantidad:
        casas_cantidad = casa
registros.close()

for _ in range(casas_cantidad):  # Filas: casas
    fila_matriz = []
    for __ in range(30):  # Columnas: días del mes
        fila_matriz.append([])
    matriz_amistades.append(fila_matriz)

for linea in registros_lista:
    perro = linea[0]
    casa = int(linea[1]) - 1
    dia = int(linea[2]) - 1

    matriz_amistades[casa][dia].append(perro)

num_casa = 1
lista_casas = []

for fila in matriz_amistades:
    ocupaciones = 0
    for col in fila:
        if len(col) > 0:
            ocupaciones += 1

    if ocupaciones < 15:
        lista_casas.append(num_casa)
    num_casa += 1

if lista_casas:
    print('Casas para vender')
    for i in lista_casas:
        print('>', i)
else:
    print('No hay casas para vender')

amistad_perros = []
amistad_existe = False

for p in perros:
    amistad = 0
    for f in matriz_amistades:
        for c in f:
            if len(c) > 1 and p in c:
                amistad += 1

    if amistad > 0:
        amistad_existe = True
    amistad_perros.append(amistad)

if amistad_existe:
    print('Los más amistosos:')
    index = 0
    for p in perros:
        if amistad_perros[index] == max(amistad_perros):
            print('>', p)
        index += 1
else:
    print('No hay amistad entre los perros')

#!/usr/bin/env python3
"""Problema 1 por David Pizarro."""
cont_linea = 0


def get_index(tubo, letra):
    """Obtiene el indice del número, si existe en la lista del tubo."""
    index = -1
    if tubo == 'T1':
        # for i, letter in enumerate(t1_config):
        for i in range(len(t1_config)):
            if letra == t1_config[i]:
                index = i - 1
    elif tubo == 'T2':
        for i in range(len(t2_config)):
            if letra == t2_config[i]:
                index = i - 1
    elif tubo == 'T3':
        for i in range(len(t3_config)):
            if letra == t3_config[i]:
                index = i - 1
    elif tubo == 'T4':
        for i in range(len(t4_config)):
            if letra == t4_config[i]:
                index = i - 1
    return index


def results(lista):
    """Imprime resultados."""
    letras = ''
    tmp_list = lista[1:]
    for i in range(0, len(tmp_list), 2):
        letras += (tmp_list[i + 1] + ' ') * int(tmp_list[i])

    print(lista[0] + ':', letras)


def evaluar(lista, nuevo):
    """Retorna True si se pueden añadir elementos."""
    usado = 0
    for i in lista:
        if str(i).isdigit():
            usado += i
    if usado + nuevo > 10:
        return_list = [10 - usado, 'restar']
    else:
        return_list = [nuevo, 'pop']
    return return_list


def tr(lista):
    """Transforma a integrers elementos de las listas."""
    for i in range(len(lista)):
        if lista[i].isdigit():
            lista[i] = int(lista[i])
    return lista


# Configuración inicial
config = open('configuracion.txt', 'r', encoding='utf-8')
linea = config.readline().strip()

while linea != '':
    cont_linea += 1
    linea_lista = linea.split(',')
    linea = config.readline().strip()
    if linea_lista[0] == 'T1':
        t1_config = tr(linea_lista[1:])
        t1_config_init = linea_lista
    elif linea_lista[0] == 'T2':
        t2_config = tr(linea_lista[1:])
        t2_config_init = linea_lista
    elif linea_lista[0] == 'T3':
        t3_config = tr(linea_lista[1:])
        t3_config_init = linea_lista
    elif linea_lista[0] == 'T4':
        t4_config = tr(linea_lista[1:])
        t4_config_init = linea_lista
config.close()

if cont_linea == 0:
    print('El archivo de texto configuracion.txt está vacío')
    exit(1)
cont_linea = 0

# Acción!
mov = open('movimientos.txt', 'r', encoding='utf-8')
linea = mov.readline().strip()

while linea != '':
    cont_linea += 1
    linea_lista = linea.split(',')
    linea = mov.readline().strip()
    desde = linea_lista[0]
    hasta = linea_lista[1]

    if desde == 'T1':
        if t1_config == []:
            continue
        indice_hasta = get_index(hasta, t1_config[-1])
        anadir = t1_config[-2]
        if hasta == 'T2':
            test = evaluar(t2_config, anadir)
            anadir = test[0]
            if indice_hasta == -1:
                t2_config += t1_config[-2:]
            else:
                t2_config[indice_hasta] += anadir
        elif hasta == 'T3':
            test = evaluar(t3_config, anadir)
            anadir = test[0]
            if indice_hasta == -1:
                t3_config += t1_config[-2:]
            else:
                t3_config[indice_hasta] += anadir
        elif hasta == 'T4':
            test = evaluar(t4_config, anadir)
            anadir = test[0]
            if indice_hasta == -1:
                t4_config += t1_config[-2:]
            else:
                t4_config[indice_hasta] += anadir

        if test[1] == 'pop':
            t1_config.pop(-1)
            t1_config.pop(-1)
        elif test[1] == 'restar':
            t1_config[-2] -= anadir
    elif desde == 'T2':
        if t2_config == []:
            continue
        indice_hasta = get_index(hasta, t2_config[-1])
        anadir = t2_config[-2]
        if hasta == 'T1':
            test = evaluar(t1_config, anadir)
            anadir = test[0]
            if indice_hasta == -1:
                t1_config += t2_config[-2:]
            else:
                t1_config[indice_hasta] += anadir
        elif hasta == 'T3':
            test = evaluar(t3_config, anadir)
            anadir = test[0]
            if indice_hasta == -1:
                t3_config += t2_config[-2:]
            else:
                t3_config[indice_hasta] += anadir
        elif hasta == 'T4':
            test = evaluar(t4_config, anadir)
            anadir = test[0]
            if indice_hasta == -1:
                t4_config += t2_config[-2:]
            else:
                t4_config[indice_hasta] += anadir

        if test[1] == 'pop':
            t2_config.pop(-1)
            t2_config.pop(-1)
        elif test[1] == 'restar':
            t2_config[-2] -= anadir
    elif desde == 'T3':
        if t3_config == []:
            continue
        indice_hasta = get_index(hasta, t3_config[-1])
        anadir = t3_config[-2]
        if hasta == 'T2':
            test = evaluar(t2_config, anadir)
            anadir = test[0]
            if indice_hasta == -1:
                t2_config += t3_config[-2:]
            else:
                t2_config[indice_hasta] += anadir
        elif hasta == 'T1':
            test = evaluar(t1_config, anadir)
            anadir = test[0]
            if indice_hasta == -1:
                t1_config += t3_config[-2:]
            else:
                t1_config[indice_hasta] += anadir
        elif hasta == 'T4':
            test = evaluar(t4_config, anadir)
            anadir = test[0]
            if indice_hasta == -1:
                t4_config += t3_config[-2:]
            else:
                t4_config[indice_hasta] += anadir

        if test[1] == 'pop':
            t3_config.pop(-1)
            t3_config.pop(-1)
        elif test[1] == 'restar':
            t3_config[-2] -= anadir
    elif desde == 'T4':
        if t4_config == []:
            continue
        indice_hasta = get_index(hasta, t4_config[-1])
        anadir = t4_config[-2]
        if hasta == 'T2':
            test = evaluar(t2_config, anadir)
            anadir = test[0]
            if indice_hasta == -1:
                t2_config += t4_config[-2:]
            else:
                t2_config[indice_hasta] += anadir
        elif hasta == 'T3':
            test = evaluar(t3_config, anadir)
            anadir = test[0]
            if indice_hasta == -1:
                t3_config += t4_config[-2:]
            else:
                t3_config[indice_hasta] += anadir
        elif hasta == 'T1':
            test = evaluar(t1_config, anadir)
            anadir = test[0]
            if indice_hasta == -1:
                t1_config += t4_config[-2:]
            else:
                t1_config[indice_hasta] += anadir

        if test[1] == 'pop':
            t4_config.pop(-1)
            t4_config.pop(-1)
        elif test[1] == 'restar':
            t4_config[-2] -= anadir
mov.close()

if cont_linea == 0:
    print('El archivo de texto movimientos.txt está vacío')
    exit(1)

results(t1_config_init)
results(t2_config_init)
results(t3_config_init)
results(t4_config_init)
print()
results(['T1'] + t1_config)
results(['T2'] + t2_config)
results(['T3'] + t3_config)
results(['T4'] + t4_config)
print()

if len(t1_config) == len(t2_config) == len(t3_config) == len(t4_config) == 2:
    print('Configuración ganadora')
else:
    print('Configuración perdedora')

#!/usr/bin/env python3
"""
Imprime las palabras más frecuentes y las veces que se repiten.

Autor: David Pizarro <david.pizarro@alumnos.ucn.cl>
"""

palabras = []
contador = []

file = open('palabras.txt', 'r', encoding='utf-8')
word = file.readline().strip()

while word != '':
    if word not in palabras:
        palabras.append(word)
        contador.append(0)
    contador[palabras.index(word)] += 1
    word = file.readline().strip()
file.close()

if len(palabras) == 0:
    print('NADA')
else:
    maximo = max(contador)
    for i in sorted(palabras):
        if contador[palabras.index(i)] == maximo:
            print(maximo, i)

#!/usr/bin/env python3
"""
Imprime una pirámide de números.

Autor: David Pizarro <david.pizarro@alumnos.ucn.cl>
"""

# Crea la lista de números
numeros = input()
numeros = numeros.split()

# Define el inicio y final del bucle
inicio = int(numeros[0])
final = int(numeros[1]) + 1
contador = 0

for i in range(final):
    for j in range(inicio, inicio + contador):
        print(j, end=' ')
    contador != 0 and print()
    if contador == final:
        break
    else:
        contador += 1

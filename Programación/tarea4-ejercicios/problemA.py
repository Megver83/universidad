#!/usr/bin/env python3
"""
Imprime SI o NO si la lista proveída es un espejo.

Autor: David Pizarro <david.pizarro@alumnos.ucn.cl>
"""

# Crea la lista
lista = input()
lista = lista.split()

# Dividela en las dos partes que debieran ser reflejos
items = len(lista)
rango = int(items/2)
mitad1 = lista[:rango]
mitad2 = lista[-rango:]

if mitad1 == mitad2[::-1] or items == 1:
    print('SI')
else:
    print('NO')

#!/usr/bin/env python3
"""
Imprime las letras más usadas.

Autor: David Pizarro <david.pizarro@alumnos.ucn.cl>
"""

cant = int(input())
lineas = []

for i in range(cant):
    string = input()
    db, quant, top = [], [], ''

    for j in string:
        if not j == ' ':
            if j not in db:
                db.append(j)
                quant.append(0)
            quant[db.index(j)] += 1

    maximo = max(quant)
    for k in sorted(db):
        if quant[db.index(k)] == maximo:
            top += k
    lineas.append(f'{top} {maximo}')

for i in lineas:
    print(i)

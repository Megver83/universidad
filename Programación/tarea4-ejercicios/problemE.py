#!/usr/bin/env python3
"""
Imprime los caminos posibles para llegar al tesoro.

Autor: David Pizarro <david.pizarro@alumnos.ucn.cl>
"""

matrix = []
fila, columna = 0, []
posible_desde, posible = [], []


def mapear(lugar, ruta):
    """Imprime la ruta tomando una lista como argumento, retorna datos."""
    energia = 0
    # puedo_llegar: 0 significa si, 1 significa no
    puedo_llegar = 0

    print('Desde', lugar + ':')
    for j in ruta:
        if j == 0:
            print('-Hierba')
        elif j == 1:
            print('-Agua')
            energia += 1
        elif j == 2:
            print('-Roca')
            energia += 2
        elif j == 3:
            print('-Fuego')
            puedo_llegar = 1

    return [lugar, energia, puedo_llegar]


file = open('valle.txt', 'r', encoding='utf-8')
line = file.readline().strip()
line = file.readline().strip()

while line != '':
    line = line.split()
    tmp_list = []

    for i in line:
        tmp_list.append(int(i))
    matrix.append(tmp_list)

    if 7 in tmp_list:
        columna_tesoro = tmp_list.index(7)
        fila_tesoro = fila

    fila += 1
    line = file.readline().strip()

file.close()

for f in matrix:
    columna.append(f[columna_tesoro])
tesoro_index = columna.index(7)

# Caminos horizontales
izq = matrix[fila_tesoro][:columna_tesoro]
der = matrix[fila_tesoro][columna_tesoro + 1:]
der = der[::-1]

# Caminos verticales
arr = columna[:tesoro_index]
aba = columna[tesoro_index + 1:]
aba = aba[::-1]

print('COORDENADAS DEL TESORO:', f'{fila_tesoro},{columna_tesoro}')
print('LOS CAMINOS:')
desde_izq = mapear('Izquierda', izq)
desde_der = mapear('Derecha', der)
desde_arr = mapear('Arriba', arr)
desde_aba = mapear('Abajo', aba)

print('PUEDO LLEGAR?')
desde = [desde_arr, desde_aba, desde_izq, desde_der]
for k in desde:
    print('Desde', k[0] + ':')
    if k[2] == 0:
        print('-Posible!')
        posible_desde.append(k[0])
        posible.append(k[1])
    elif k[2] == 1:
        print('-No es posible!')

print('MEJOR CAMINO:')
if len(posible) == 0:
    print('-NINGUNO')
else:
    print('-Desde', posible_desde[posible.index(min(posible))])
